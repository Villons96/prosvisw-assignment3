### Processo e sviluppo software
#### Villa Giacomo 807472
##### Informazioni di sviluppo
Per sviluppare questo assigment è stato utilizzato:
* mySql 5.7.24
* Eclispe Java EE IDE for Web Developers 2018-19 4.9.0
* [MySql connector 8.0.13](https://dev.mysql.com/downloads/connector/j/8.0.html)
##### Istruzioni
1. Aprire il terminale e digitare "*git clone https://gitlab.com/Villons96/prosvisw-assignment3.git*"
2. Aprire Eclise
3. *File > New > JPA Project*
4. Selezionare JPA version 2.1 
5. Alla pagina *JPA Facet* selezionare download library e installare EclipseLink 2.5.2
6. Nel progetto così creato:
    *  tasto destro *Build Path > Configure Build Path > Libraries > **1** > Add Library > JUnit > JUnit4 > Finish > Apply and Close*
    *  tasto destro *Build Path > Configure Build Path > Libraries > **1** > Add External JARs > selezionare i MySqlConnector.jar > Apply and Close*
    * tasto destro *src > Import > File System > Browse >* selezionare dunque *src* presente nella repo scaricata; cliccare la check box al fianco nel menù *Import* di eclipse e cliccare *Finish*, alla domanda per sovrascrivere il persistence.xml cliccare "*Yes, to all*"
7. Nel file *src/META-INF/persistence.xml/Source* modificare *USER* e *PSW* con le proprie credenziali root dato MySql
8. Tasto destro su *src > staple > TheTest.java* selezionare *Run As JUnit Test*

In caso di qualsiasi problema sono pronto a presentarmi con il computer con cui ho sviluppato per una eventuale demo mostrando i passaggi eseguiti sul mio computer. 

**1** Eventualmente selezionare con click sinistro Classpath