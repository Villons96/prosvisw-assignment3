package operation;

import java.util.List;

import java.util.Vector;
import chara.Building;

import javax.persistence.EntityManager;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import chara.Company;

// I commenti toccano solo la prima occorrenza dell'istruzione commentata
public class CompanyOperation {
	
	// Metodo Create, dato oggetto entry si occuperà di inserirlo all'interno del db
	public static int create(Company entry) {
		
		// Apertura Entity Manager
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		// Apertura transazione 
		em.getTransaction().begin();
		
		// Inserimento nel database
		em.merge(entry);
		
		// Commit transazione
		em.getTransaction().commit();
		
		// Chiusura Entity Manager
		em.close();
		factory.close();
			
		return 1;
		
	}

	// Metodo Read, dato identificativo recupera dal db la compagnia e restituisce la stessa
	public static Company read(long compId) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
			
		// Lettura dell'oggetto dal db
		Company c = em.find(Company.class, compId);
		em.close();
		factory.close();
		
		return c;
		
	}			

	// Metodo Update del nome, data la compagnia identificata dall'id modifica il nome
	public static int updateName(long compId, String newName) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
			
		// Lettura dell'oggetto dal db mediante metodo read
		Company c = read(compId);
		
		// Modifica il nome con metodi della classe 
		c.setCompName(newName);

		em.getTransaction().begin();
		
		// Inserimento mediante merge permette di modificare senza creare nuova tupla
		// se la tupla già precedentemente esisteva
		em.merge(c);
		em.getTransaction().commit();
		em.close();
		factory.close();
		
		return 1;
		
	}

	// Metodo Update della nazionalità, data la compagnia identificata dall'id modifica 
	// la nazionalità
	public static int updateNationality(long compId, String newNationality) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
			
		Company c = read(compId);
		c.setCompNationality(newNationality);
		
		em.getTransaction().begin();
		em.merge(c);
		em.getTransaction().commit();
		em.close();
		factory.close();
		
		return 1;		
	}
	
	// Metodo Delete della compagnia corrispondente all'identificativo passato
	public static int delete(long compId) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		// Istanziazione di un vettore contenente tutti i cantieri presenti nel db
		Vector<Building> build = (Vector<Building>) BuildingOperation.selectAllBuilding();
		
		// Istanziazione di un vettore per memorizzare, dato un building, tutte le compagnie
		// finanziatrici compresa quella che si intende eliminare
		Vector<Company> funded = new Vector<Company>();
		
		// Istanziazione di un vettore per memorizzare, dato un building, tutte le compagnie 
		// finanziatrici tolta quella che voglio eliminare
		Vector<Company> mantein = new Vector<Company>();
		Company c = read(compId);
		
		for(int i = 0; i < build.size(); i++) {
			
			// Dato l'iesimo building recupero tutte le compagnie finanziatrici
			funded = (Vector<Company>) build.get(i).getFunded();
			for(int j = 0; j < funded.size(); j++) {
				
				// Date le compagnie finanziatrici estraggo tutte tranne, eventualmente,
				// quella che voglio elimianre
				if(funded.get(j).getCompId() != c.getCompId()) 
					mantein.add(funded.get(j));
			}
			
			// Se il building continua ad avere compagnie finanziatrici non elimino 
			// ed eseguo un update così nel caso di eliminazione della compagnia non ho problemi
			// di vincoli esterni
			if(mantein.size() != 0)
				BuildingOperation.updateFunded(build.get(i).getBuildId(), mantein);
			
			// Se il building non ha più compagnie finaziatrici viene eliminato
			else
				BuildingOperation.delete(build.get(i).getBuildId());
			
			funded.clear();
			mantein.clear();
		}
		
		em.getTransaction().begin();
		
		// Elimino la compagnia
		if (!em.contains(c)) {
			c = em.merge(c);
		}
		em.remove(c);
		em.getTransaction().commit();
		em.close();
		factory.close();	
	
		return 1;
		
		}
	
	// Seleziono tutte le compagnie del db
	public static List<Company> selectAllCompany() {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		// Eseguo la query che seleziona tutte le compagnie del db e richiedo una lista 
		// per quanto riguarda il risultato
		List<Company> listOfCompany = (List<Company>) em.createQuery("SELECT c FROM chara.Company c", Company.class).getResultList();
		
		return listOfCompany;
	}
	
	// Seleziono tutte le compagnie di una data nazione
	public static List<Company> selectByNationality(String cNationality) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		// Dichiaro l'oggetto query che conterrà la query che voglio performare
		Query query;
		
		// Creo una query per la selezione posizionado un "segnaposto" per il parametro
		query = em.createQuery("SELECT c FROM chara.Company c WHERE c.compNationality = :cNationality", Company.class);
		
		// Setto il "segnaposto" uguale al parametro passato
		query.setParameter("cNationality", cNationality);	
		
		// Eseguo la query e richiedo il risultato sotto forma di lista
		List<Company> listOfCompany = (List<Company>) query.getResultList();
		
		return listOfCompany;
	}
}
