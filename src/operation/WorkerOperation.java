package operation;

import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import chara.Building;
import chara.Worker;

// I commenti toccano solo la prima occorrenza dell'istruzione commentata
public class WorkerOperation {
	
	// Metodo Create, dato oggetto entry si occuperà di inserirlo all'interno del db
	public static int create(Worker entry) {
		
		// Apertura Entity Manager
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		// Apertura transazione 
		em.getTransaction().begin();
	
		// Inserimento nel database
		em.merge(entry);
		
		// Commit transazione
		em.getTransaction().commit();
		
		// Chiusura Entity Manager
		em.close();
		factory.close();
			
		return 1;
		
	}

	// Metodo Read, dato identificativo recupera dal db il worker e restituisce lo stesso
	public static Worker read(long workId) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		Worker w = new Worker();
		
		// Lettura dell'oggetto dal db
		w = em.find(Worker.class, workId);
		em.close();
		factory.close();
		return w;
	}			
	
	// Metodo Update del nome, data il worker identificato dall'id modifica il nome
	public static int updateName(long workId, String newName) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
			
		// Lettura dell'oggetto dal db mediante metodo read
		Worker w = read(workId);
		
		// Modifica il nome con metodi della classe 
		w.setName(newName);
			
		em.getTransaction().begin();
		
		// Inserimento mediante merge permette di modificare senza creare una nuova tupla
		// se la tupla già precedentemente esisteva
		em.merge(w);
		em.getTransaction().commit();
		em.close();
		factory.close();
		return 1;
		
	}

	// Metodo Update del surname, data il worker identificato dall'id modifica
	// il surname
	public static int updateSurname(long workId, String newSurname) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
			
		Worker w = read(workId);
		w.setSurname(newSurname);
			
		em.getTransaction().begin();
		em.merge(w);
		em.getTransaction().commit();
		em.close();
		factory.close();
		return 1;
		
	}
	
	// Metodo Update della data di nasciata, data il worker identificato dall'id modifica
	// la data di nascita
	public static int updateBornDate(long workId, GregorianCalendar newBornDate) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
	
		Worker w = read(workId);
		
		// Istanzia la nuova data 
		w.setBornDate(newBornDate);
	
		em.getTransaction().begin();
		em.merge(w);
		em.getTransaction().commit();
		em.close();
		factory.close();
		return 1;
	}
	
	// Metodo Update del chief, data il worker identificato dall'id modifica
	// il suo capo
	public static int updateChief(long workId, Worker nChief) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
	
		Worker w = read(workId);
		w.setChief(nChief);
	
		em.getTransaction().begin();
		em.merge(w);
		em.getTransaction().commit();
		em.close();
		factory.close();
		
		return 1;
	}
	
	// Metodo Update del build, data il worker identificato dall'id modifica
	// il posto di lavoro
	public static int updateBuild(long workId, Building b) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		// Viene istanziato una lista per identificare, nel caso in cui il worker fosse un chief,
		// la squadra.
		List<Worker> team = selectAllTeam(workId);
	
		Worker w = read(workId);
		w.setBuildingSite(b);
	
		em.getTransaction().begin();
		em.merge(w);
		em.getTransaction().commit();
		em.close();
		factory.close();
		
		// Per tutti gli eventuali componenti della squadra del lavoratore viene modificato
		// il posto di lavoro. Il ciclo viene eseguito solo se il worker risultava essere
		// un chief
		for(int i = 1; i < team.size(); i++) 
			WorkerOperation.updateBuild(team.get(i).getWoId(), b);
		
		return 1;
		
	}

	// Metodo Delete del worker corrispondente all'identificativo passato
	public static int delete(long workId) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		// Viene istanziata una lista di lavoratori così, nel caso in cui il lavoratore in questione
		// fosse un capo, si garantisce la consistenza eliminando tutta la squadra come da specifica
		// deciso
		List<Worker> team = selectAllTeam(workId);
		
		// Per ogni eventuale lavoratore subbordinato si setta a null le chiavi esterne
		// per evitare problemi di chiavi 
		for(int i = 1; i < team.size(); i++) {
			WorkerOperation.updateBuild(team.get(i).getWoId(), null);
			WorkerOperation.updateChief(team.get(i).getWoId(), null);
		}
		
		Worker w = read(workId);
		
		// Setto a null le chiavi del lavoratore che voglio eliminare
		w.setChief(null);
		w.setBuildingSite(null);
		em.getTransaction().begin();
		
		// Elimino il lavoratore
		if (!em.contains(w)) {
		    w = em.merge(w);
		}
		em.remove(w);
		em.getTransaction().commit();
		em.close();
		factory.close();			

		// Procedura lanciata per garantire la consistenza, elimina tutti i lavoratori
		// con chiavi esterne uguali a null
		deleteAllNullWorkerChief();
		
		return 1;
		
	}
	
	// Query utilizzara per garantiere la consistenza, elimina tutti i lavoratori con 
	// chiavi esterne uguali a null
	public static int deleteAllNullWorkerChief() {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		// Dichiaro l'oggetto query che conterrà la query che voglio performare
		Query query;
		
		// Eseguo in un blocco try/catch l'esecizione della query
		try {
			em.getTransaction().begin();
			
			// Creo la query
			query = em.createQuery("DELETE FROM chara.Worker e WHERE e.chief IS NULL");
			
			// eseguo la query
			query.executeUpdate();
			em.getTransaction().commit();
			
			return 1;
		}catch (Exception e) {
			return -1;
		}	
		
	}
	
	
	public static List<Worker> selectAllWorkerInBuilding(long buildId) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		Query query;
		
		// Creo una query per la selezione posizionado un "segnaposto" per il parametro
		query = em.createQuery("SELECT w FROM chara.Worker w WHERE w.buildingSite = :buildId", Worker.class);
		
		// Setto il "segnaposto" uguale al parametro passato
		query.setParameter("buildId", BuildingOperation.read(buildId));	
		
		List<Worker> workerInBuilding = (List<Worker>) query.getResultList();
				
		return workerInBuilding;
	}
	
	// Seleziono tutti i lavoratori che hanno come capo il worker identificato dal valore passato
	public static List<Worker> selectAllTeam(long chiefId) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		Query query;
		
		query = em.createQuery("SELECT w FROM chara.Worker w WHERE w.chief = :chiefId", Worker.class);
		query.setParameter("chiefId", WorkerOperation.read(chiefId));	
		
		List<Worker> team = (List<Worker>) query.getResultList();
				
		return team;
	}
	
	// Seleziono tutti i lavoratori del db
	public static List<Worker> selectAllWorker() {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		List<Worker> listOfWorker = (List<Worker>) em.createQuery("SELECT w FROM chara.Worker w", Worker.class).getResultList();
		
		return listOfWorker;
	}
	
	// Seleziono tutti i lavoratori dotati di nome e cognome pari a quelli passati come
	// parametri
	public static List<Worker> selectByNameAndSurname(String wName, String wSurname) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		Query query;
		
		query = em.createQuery("SELECT w FROM chara.Worker w WHERE w.name = :wName AND w.surname = :wSurname", Worker.class);
		query.setParameter("wName", wName);	
		query.setParameter("wSurname", wSurname);	
		
		List<Worker> listOfWorker = (List<Worker>) query.getResultList();
				
		return listOfWorker;
	}

}
