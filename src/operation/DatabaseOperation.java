package operation;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

// I commenti toccano solo la prima occorrenza dell'istruzione commentata
public class DatabaseOperation {

	// Metodo di supporto utilizzato nei test delle singole entità per ripulire il database
	public static int deleteAllTuple(){
		
		// Apertura Entity Manager
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		// Dichiaro l'oggetto query che conterrà la query che voglio performare
		Query query;
		
		// Inserisco in un blocco try/catch l'esecuzione delle eliminazioni delle varie tuple
		try {
			// Apertura transazione 
				em.getTransaction().begin();
				query = em.createQuery("DELETE FROM chara.Worker e WHERE e.woId IS NOT NULL");
				query.executeUpdate();
				query = em.createQuery("DELETE FROM chara.Building e WHERE e.buildId IS NOT NULL");
				query.executeUpdate();
				query = em.createQuery("DELETE FROM chara.Company e WHERE e.compId IS NOT NULL");
				query.executeUpdate();
				query = em.createQuery("DELETE FROM chara.Neighborhood e WHERE e.neighId IS NOT NULL");
				query.executeUpdate();
				
				// Commit transazione
				em.getTransaction().commit();
				
				// Chiusura Entity Manager
				em.close();
				factory.close();
				return 1;
			}
			catch (Exception e) {
				return -1;
		}	
	}
}
