package operation;

import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;

import javax.persistence.EntityManager;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import chara.Building;
import chara.Company;
import chara.Neighborhood;
import chara.Worker;

// I commenti toccano solo la prima occorrenza dell'istruzione commentata
public class BuildingOperation {
	
	// Metodo Create, dato oggetto entry si occuperà di inserirlo all'interno del db
	public static int create(Building entry) {
		
		// Apertura Entity Manager
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		// Apertura transazione 
		em.getTransaction().begin();
		
		// Inserimento nel database
		em.merge(entry);
		
		// Commit transazione
		em.getTransaction().commit();
		
		// Chiusura Entity Manager
		em.close();
		factory.close();
			
		return 1;	
		
	}
	
	// Metodo Read, dato identificativo recupera dal db il building e restituisce lo stesso
	public static Building read(long buildId) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		Building b = new Building();
		
		// Lettura dell'oggetto dal db
		b = em.find(Building.class, buildId);
		em.close();
		factory.close();
		
		return b;
		
	}			

	// Metodo Update della tipologia, data il building identificato dall'id modifica il nome
	public static int updateTypology(long buildId, String newTypology) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		Building b = new Building();
		
		// Lettura dell'oggetto dal db mediante metodo read
		b = read(buildId);
		
		// Modifica il nome con metodi della classe 
		b.setTypology(newTypology);
		
		em.getTransaction().begin();
		
		// Inserimento mediante merge permette di modificare senza creare una nuova tupla
		// se la tupla già precedentemente esisteva
		em.merge(b);
		em.getTransaction().commit();
		em.close();
		factory.close();
		
		return 1;
		
	}
	
	// Metodo Update della data di inizio lavori, dato il building con identificativo
	// pari al valore passato
	public static int updateSDate(long buildId, GregorianCalendar newDate) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		Building b = read(buildId);
		b.setStartDate(newDate);
		
		em.getTransaction().begin();
		em.merge(b);
		em.getTransaction().commit();
		em.close();
		factory.close();
		
		return 1;
		
	}	
	
	// Metodo Update della data di fine lavori, dato il building con identificativo
	// pari al valore passato
	public static int updateEDate(long buildId, GregorianCalendar newDate) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
			
		Building b = read(buildId);
		b.setEndDate(newDate);
			
		em.getTransaction().begin();
		em.merge(b);
		em.getTransaction().commit();
		em.close();
		factory.close();
		
		return 1;
		
	}
		
	// Metodo Update delle compagnie finanziatrici, dato un building idefinticato dal
	// valore passato
	public static int updateFunded(long buildId, Vector<Company> c) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
			
		Building b = read(buildId);
		b.setFunded(c);
		try {		
			em.getTransaction().begin();
			em.merge(b);
			em.getTransaction().commit();
			em.close();
			factory.close();
			return 1;
		}catch(Exception e) {
			return -1;
		}
		
	}
		
	// Metodo Update della location, dato il building con identificativo
	// pari al valore passato
	public static int updateLocation(long buildId, Neighborhood n) {
			
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
			
		Building b = read(buildId);
		b.setLocation(n);;
			
		em.getTransaction().begin();
		em.merge(b);
		em.getTransaction().commit();
		em.close();
		factory.close();
		
		return 1;
		
	}
		
	// Metodo Delete del building corrispondente all'identificativo passato	
	public static int delete(long buildId) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		// Istanziazione di una lista contenente i lavoratori che lavorano nel building che
		// intendo eliminare così da settare a null le chiavi esterne
		List<Worker> workerInBuild = WorkerOperation.selectAllWorkerInBuilding(buildId);
		for(int i = 0; i < workerInBuild.size(); i++) {
			WorkerOperation.updateBuild(workerInBuild.get(i).getWoId(), null);
			WorkerOperation.updateChief(workerInBuild.get(i).getWoId(), null);
		}
				
		Building b = read(buildId);
		
		// Setto a null la chiave esterna per evitare errori legati a questa
		updateLocation(buildId, null);
		
		em.getTransaction().begin();
		
		// Elimino il lavoratore
		if (!em.contains(b)) {
		    b = em.merge(b);
			}
		em.remove(b);
		em.getTransaction().commit();
		em.close();
		factory.close();	
		
		// Procedura lanciata per garantire la consistenza, elimina tutti i lavoratori
		// con chiavi esterne uguali a null
		WorkerOperation.deleteAllNullWorkerChief();
				
		return 1;
		
	}	
	
	// Seleziono tutti i building del db
	public static List<Building> selectAllBuilding() {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		// Eseguo la query che seleziona tutte i building del db e richiedo una lista 
		// per quanto riguarda il risultato
		List<Building> listOfBuilding = (List<Building>) em.createQuery("SELECT b FROM chara.Building b", Building.class).getResultList();
		
		return listOfBuilding;
	}
	
	// Seleziona tutti i cantieri nel quartiere identificato dal valore passato
	public static List<Building> selectAllBuildingInNeighborhood(long neighId) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		// Dichiaro l'oggetto query che conterrà la query che voglio performare
		Query query;
		
		// Creo una query per la selezione posizionado un "segnaposto" per il parametro
		query = em.createQuery("SELECT b FROM chara.Building b WHERE b.location = :neighId", Building.class);
		
		// Setto il "segnaposto" uguale al parametro passato
		query.setParameter("neighId", NeighborhoodOperation.read(neighId));	
		
		List<Building> builds = (List<Building>) query.getResultList();
				
		return builds;
	}

	// Seleziono tutti i bulding finanziati da una certa compagnia
	public static List<Building> selectFundedBy(long compId) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		Query query;
		
		query = em.createQuery("SELECT b FROM chara.Building b WHERE b.funded = :compId", Building.class);
		query.setParameter("compId", CompanyOperation.read(compId));
		
		List<Building> build = (List<Building>) query.getResultList();
		
		return build;
	}
	
	// Seleziono tutti i building che hanno iniziato i lavori dopo una certa data passata
	// come parametro
	public static List<Building> getBuildStartIn(GregorianCalendar sDate) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		Query query;
		
		query = em.createQuery("SELECT b FROM chara.Building b WHERE b.startDate >= :sDate", Building.class);
		query.setParameter("sDate", sDate);
		
		List<Building> build = (List<Building>) query.getResultList();
		
		return build;
	}
	
	// Seleziono tutti i building data la tipologia passata come parametro
	public static List<Building> selectByTypology(String typology) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		Query query;
		
		query = em.createQuery("SELECT b FROM chara.Building b WHERE b.typology = :typology", Building.class);
		query.setParameter("typology", typology);
		
		List<Building> build = (List<Building>) query.getResultList();
		
		return build;
	}
}
