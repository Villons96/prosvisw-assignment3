package operation;

import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import chara.Building;
import chara.Neighborhood;

//I commenti toccano solo la prima occorrenza dell'istruzione commentata
public class NeighborhoodOperation {
	
	// Metodo Create, dato oggetto entry si occuperà di inserirlo all'interno del db
	public static int create(Neighborhood entry) {
		
		// Apertura Entity Manager
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		// Apertura transazione 
		em.getTransaction().begin();
		
		// Inserimento nel database
		em.merge(entry);
		
		// Commit transazione
		em.getTransaction().commit();
		
		// Chiusura Entity Manager
		em.close();
		factory.close();
			
		return 1;
		
	}
	
	// Metodo Read, dato identificativo recupera dal db il quartiere e restituisce lo stesso
	public static Neighborhood read(long neighId) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		Neighborhood n = new Neighborhood();
		
		// Lettura dell'oggetto dal db
		n = em.find(Neighborhood.class, neighId);
		em.close();
		factory.close();
		return n;
		
	}
	
	// Metodo Update del nome, data il quartiere identificato dall'id modifica il nome
	public static int updateName(long neighId, String newName) {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
			
		// Lettura dell'oggetto dal db mediante metodo read
		Neighborhood n = read(neighId);
		
		// Modifica il nome con metodi della classe 
		n.setNeighName(newName);
		
		em.getTransaction().begin();
		
		// Inserimento mediante merge permette di modificare senza creare una nuova tupla
		// se la tupla già precedentemente esisteva
		em.merge(n);
		em.getTransaction().commit();
		em.close();
		factory.close();
		return 1;		
	}
	
	// Metodo Update della popolazione, data il quartiere identificato dall'id modifica
	// la popolazione
	public static int updatePopulation(long neighId, int newPopulation) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
				
		Neighborhood n = read(neighId);
		n.setPopulation(newPopulation);
		
		em.getTransaction().begin();
		em.merge(n);
		em.getTransaction().commit();
		em.close();
		factory.close();
		
		return 1;
		
	}
	
	// Metodo Delete della quartiere corrispondente all'identificativo passato
	public static int delete(long neighId) {
			
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		// Istanziazione della lista contentente tutti i cantiere che hanno luogo nel 
		// quartiere che si vuole eliminare
		List<Building> buildInNeigh = BuildingOperation.selectAllBuildingInNeighborhood(neighId);
		
		// Per ogni cantiere che ha luogo nel quartiere viene settato a null la chiave esterna 
		// così da evitare errori di chiavi esterne e in seguito viene eliminato
		for(int i = 0; i < buildInNeigh.size(); i++) {
			BuildingOperation.updateLocation(buildInNeigh.get(i).getBuildId(), null);
			BuildingOperation.delete(buildInNeigh.get(i).getBuildId());
		}
		
		Neighborhood n = read(neighId);
			
		em.getTransaction().begin();
		
		// Elimino il quartiere
		if (!em.contains(n)) {
		    n = em.merge(n);
		}
		em.remove(n);
		em.getTransaction().commit();
		em.close();
		factory.close();			
				
		return 1;
		
	}
	
	// Seleziono tutti i quartieri del db
	public static List<Neighborhood> selectAllNeighborhood() {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		// Eseguo la query che seleziona tutte i quartieri del db e richiedo una lista 
		// per quanto riguarda il risultato
		List<Neighborhood> listOfNeighborhood = (List<Neighborhood>) em.createQuery("SELECT n FROM chara.Neighborhood n", Neighborhood.class).getResultList();
		
		return listOfNeighborhood;
	}
	
	// Seleziono il quartiere identificato dal nome passato come parametro 
	public static Neighborhood selectByName(String nName) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Assigment3");
		EntityManager em = factory.createEntityManager();
		
		// Dichiaro l'oggetto query che conterrà la query che voglio performare
		Query query;
		
		// Creo una query per la selezione posizionado un "segnaposto" per il parametro
		query = em.createQuery("SELECT n FROM chara.Neighborhood n WHERE n.neighName = :nName", Neighborhood.class);
		
		// Setto il "segnaposto" uguale al parametro passato
		query.setParameter("nName", nName);
		
		List<Neighborhood> listOfNeighborhood = (List<Neighborhood>) query.getResultList();
		
		if(listOfNeighborhood.isEmpty())
			return null;
		
		return(listOfNeighborhood.get(0));
	}

}
