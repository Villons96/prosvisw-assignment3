package service;

import java.util.GregorianCalendar;
import java.util.List;

import chara.Building;
import chara.Worker;
import operation.BuildingOperation;
import operation.DatabaseOperation;
import operation.WorkerOperation;

// I commenti toccano solo la prima occorrenza dell'istruzione commentata
public class WorkerService {

	// Questo metodo permette di inserire un nuovo worker
	public static int insert(String wName, String wSurname, int wBDay, int wBMonth, int wBYear, long nChief, long nBuild) {
		
		GregorianCalendar wBornDate;
		Building b;
		Worker wChief;
		Worker w;
		
		// Controlla che il nome non sia una stringa vuota
		if(wName.equals("")) {
			//System.out.println("Name field is empty, insert a non-empty name");
			return -1;
		}
		
		// Controlla che il cognome non sia una stringa vuota
		if(wSurname.equals("")) {
			//System.out.println("Surname field is empty, insert a non-empty surname");
			return -2;
		}
		
		// Controlla che i parametri dalla data abbiano senso
		if(wBDay <= 0 || wBDay >= 32 || wBMonth < 0 || wBMonth >= 13 || wBYear < 0) {
			//System.out.println("Day or month in BornDate is not correct");
			return -3;
		}
		
		// Controlla che il lavoratore sia maggiorenne
		if(GregorianCalendar.getInstance().get(GregorianCalendar.YEAR) - wBYear < 18) {
			//System.out.println("A worker must be a adult");
			return -4;
		}
		
		// Recupa il capo dato l'identificativo passato
		wChief = WorkerOperation.read(nChief);
		
		// Recupera il posto di lavoro dato l'identificativo di questo
		b = BuildingOperation.read(nBuild);
		
		// Istanzia l'oggetto che rappresenta la data
		wBornDate = new GregorianCalendar(wBYear, wBMonth, wBDay);
		
		// Istanzia il lavoratore
		w = new Worker(wName, wSurname, wBornDate, wChief, b);
		
		// Controlla che il lavoratore sia stato correttamente creato
		if(WorkerOperation.create(w) == 1) {
			//System.out.println("Done!");
			return 1;
		}
		else {
			//System.out.println("Something went wrong during creation process, see log for more details");
			return -5;
		}
	}
	
	// Permette di recuperare il lavoratore identificato dal parametro passato
	public static Worker retrieve(long wId) {
		
		// Legge dal db
		Worker w = WorkerOperation.read(wId);
		return w;
	}
	
	// Metodo per eseguire l'update del nome del lavoratore identificato dal parametro
	// passato
	public static int updateName(long wId, String nWName) {
		
		if(nWName.equals("")) {
			//System.out.println("Name field is empty, insert a non-empty name");
			return -1;
		}
		
		// Verifica che il lavoratore che si intende modificare sia presente nel db
		if(retrieve(wId) == null) {
			//System.out.println("The entity is not in the database");
			return -2;
		}
		
		// Verifica che la modifica sia andata a buon fine
		if(WorkerOperation.updateName(wId, nWName) == 1) {
			//System.out.println("Done!");
			return 1;
		}
		else {
			//System.out.println("Something went wrong during update name process, see log for more details");
			return -3;
		}
	}
	
	// Metodo per eseguire l'update del cognome del lavoratore identificato dal parametro
	// passato 
	public static int updateSurname(long wId, String nWSurname) {
		
		if(nWSurname.equals("")) {
			//System.out.println("Surname field is empty, insert a non-empty surname");
			return -1;
		}
		
		if(retrieve(wId) == null) {
			//System.out.println("The entity is not in the database");
			return -2;
		}
		
		if(WorkerOperation.updateSurname(wId, nWSurname) == 1) {
			//System.out.println("Done!");
			return 1;
		}
		else {
			//System.out.println("Something went wrong during update surname process, see log for more details");
			return -3;
		}
	}

	// Metodo per eseguire l'update della data di nascita del lavoratore identificato dal parametro
	// passato
	public static int updatewBDate(long wId, int nYear, int nMonth, int nDay) {
	
		if(nDay <= 0 || nDay >= 32 || nMonth < 0 || nMonth >= 13 || nYear < 0) {
			//System.out.println("The new date has a bad format");
			return -1;
		}
	
		if(retrieve(wId) == null) {
			//System.out.println("The entity is not in the database");
			return -2;
		}
	
		if((GregorianCalendar.getInstance().get(GregorianCalendar.YEAR) - nYear) < 18) {
			//System.out.println("New date don't respect adult constraint");
			return -3;
		}
	
		GregorianCalendar nBDate = new GregorianCalendar(nYear, nMonth, nDay);
		if(WorkerOperation.updateBornDate(wId, nBDate) == 1) {
			//System.out.println("Done!");
			return 1;
		}
		else {
			//System.out.println("Something went wrong during update newDate process, see log for more details");
			return -4;
		}
	}
	
	// Metodo per eseguire l'update del cantiere del lavoratore identificato dal parametro
	// passato
	public static int updateBuild(long wId, long bId) {
		
		if(retrieve(wId) == null) {
			//System.out.println("The entity is not in the database");
			return -1;
		}
		
		if(WorkerOperation.updateBuild(wId, BuildingOperation.read(bId)) == 1) {
			//System.out.println("Done!");
			return 1; 
		}
		else {
			//System.out.println("Something went wrong during update Build process, see log for more details");
			return -2;
		}
	}
	
	// Metodo per eseguire l'update del capo del lavoratore identificato dal parametro
	// passato
	public static int updateChief(long wId, long wCId) {
		
		if(retrieve(wId) == null) {
			//System.out.println("The entity is not in the database");
			return -1;
		}
		
		if(WorkerOperation.updateChief(wId, retrieve(wCId)) == 1) {
			//System.out.println("Done!");
			return 1; 
		}
		else {
			//System.out.println("Something went wrong during update Chief process, see log for more details");
			return -2;
		}
	}
	
	// Metodo per eseguire il remove del lavoratore identificato dal parametro passato
	public static int remove(long wId) {
		
		if(retrieve(wId) == null) {
			//System.out.println("The entity is not in the database");
			return -1;
		}
		
		if(WorkerOperation.delete(wId) == 1) {
			//System.out.println("Done!");
			return 1; 
		}
		else {
			//System.out.println("Something went wrong during delete process, see log for more details");
			return -2;
		}
	}
	
	// Operazioni di selezione
	public static List selectAllWorkerInBuilding(long buildId) {
		
		return(WorkerOperation.selectAllWorkerInBuilding(buildId));
	}
	
	public static List selectAllTeam(long wId) {
		
		return(WorkerOperation.selectAllTeam(wId));
	}
	
	public static List selectAllWorker() {
		
		return(WorkerOperation.selectAllWorker());
	}
	
	public static List selectWorkerByNameAndSurname(String wName, String wSurname) {
		
		if(wName.equals("") || wSurname.equals("")) {
			//System.out.println("Name or Surname field is empty, insert a non-empty name or Surname");
			return null;
		}
		
		return(WorkerOperation.selectByNameAndSurname(wName, wSurname));	
	}
}
