package service;

import java.util.List;

import chara.Neighborhood;
import operation.DatabaseOperation;
import operation.NeighborhoodOperation;

// I commenti toccano solo la prima occorrenza dell'istruzione commentata
public class NeighborhoodService {

	// Questo metodo permette di inserire un nuovo neighborhood
	public static int insert(String nName, int npopulation) {
		
		// Controlla che il nome non sia vuoto
		if(nName.equals("")) {
			//System.out.println("Name field is empty, insert a non-empty name");
			return -1;
		}
		
		// Controlla che la popolazione non sia un valore negativo
		if(npopulation <= 0) {
			//System.out.println("population field is negative number, insert a non-negative number");
			return -2;
		}
		
		Neighborhood n = new Neighborhood(nName, npopulation);
		
		// Controlla che il neighborhood sia stata correttamente creato
		if(NeighborhoodOperation.create(n) == 1) {
			//System.out.println("Done!");
			return 1;
		}	
		else {
			//System.out.println("Something went wrong during creation process, see log for more details");
			return -3;
		}				
	}
	
	// Permette di recuperare il neighborhood identificato dal parametro passato
	public static Neighborhood retrieve(long nId) {
		
		Neighborhood n = NeighborhoodOperation.read(nId);
		return n;
	}
	
	// Metodo che permette di moficare il nome del quartiere identificato dal parametro
	public static int updateName(long nId, String nName) {
		
		if(nName.equals("")) {
			//System.out.println("Name field is empty, insert a non-empty name");
			return -1;
		}
		
		// Verifica che il quartiere che si intende modificare sia presente nel db
		if(NeighborhoodOperation.read(nId) == null) {
			//System.out.println("The entity is not in the database");
			return -2;
		}
		
		// Verifica che la modifica sia andata a buon fine
		if(NeighborhoodOperation.updateName(nId, nName) == 1) {
			//System.out.println("Done!");
			return 1;
		}
		else {
			//System.out.println("Something went wrong during update name process, see log for more details");
			return -3;
		}		
	}
	
	// Metodo che permette di moficare la popolazione del quartiere identificato dal parametro
	public static int updatepopulation(long nId, int npopulation) {
		
		if(npopulation <= 0) {
			//System.out.println("population field is negative number, insert a non-negative number");
			return -1;
		}
		
		if(NeighborhoodOperation.read(nId) == null) {
			//System.out.println("The entity is not in the database");
			return -2;
		}
		
		if(NeighborhoodOperation.updatePopulation(nId, npopulation) == 1) {
			//System.out.println("Done!");
			return 1;
		}
		else {
			//System.out.println("Something went wrong during update population process, see log for more details");
			return -3;
		}		
	}
	
	// Metodo per eseguire il remove del neigborhood identificata dal parametro passato
	public static int remove(long nId) {
		
		if(NeighborhoodOperation.read(nId) == null) {
			//System.out.println("The entity is not in the database");
			return -1;
		}
		
		if(NeighborhoodOperation.delete(nId) == 1) {
			//System.out.println("Done!");
			return 1;
		}
		else {
			//System.out.println("Something went wrong during delete process, see log for more details");
			return -3;
		}
	}
	
	// Operazioni di selezione
	public static List selectAllNeighborhood() {
		
		return(NeighborhoodOperation.selectAllNeighborhood());
	}
	
	public static Neighborhood selectNeighborhoodByName(String nName) {
		
		if(nName.equals("")) {
			//System.out.println("Name field is empty, insert a non-empty name");
			return null;
		}
		
		return(NeighborhoodOperation.selectByName(nName));
	}
}
