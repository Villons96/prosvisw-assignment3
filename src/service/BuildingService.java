package service;

import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;

import chara.Building;
import chara.Company;
import chara.Neighborhood;
import operation.BuildingOperation;
import operation.CompanyOperation;
import operation.DatabaseOperation;
import operation.NeighborhoodOperation;

// I commenti toccano solo la prima occorrenza dell'istruzione commentata
public class BuildingService {

	// Questo metodo permette di inserire un nuovo building
	public static int insert(long nId, String bTyp, int day1, int month1, int year1, int day2, int month2, int year2) {
		
		GregorianCalendar sDate;
		GregorianCalendar eDate;
		Building b;
		Neighborhood n = NeighborhoodOperation.read(nId);
		
		// Verifica la tipologia non sia una stringa vuota
		if(bTyp.equals("")) {
			//System.out.println("bTyp field is empty, insert a non-empty name");
			return -1;
		}
		
		// Controlla che i parametri della data abbiano senso
		if(day1 <= 0 || day1 >= 32 || month1 < 0 || month1 >= 13 || year1 < 0) {
			//System.out.println("Day or month in StartDate is not correct");
			return -2;
		}
		
		if(day2 <= 0 || day2 >= 32 || month2 < 0 || month2 >= 13 || year2 < 0) {
			//System.out.println("Day or month in EndDate is not correct");
			return -3;
		}
		
		// Verifica che la data di inizio lavori sia antecedente a quella di fine
		if((year2 - year1 == 0 && month2 - month1 == 0 && day2 - day1 <= 0) || (year2 - year1 < 0)) {
			//System.out.println("Bad relationship between StartDate and EndDate");
			return -4;
		}
	
		sDate = new GregorianCalendar(year1, month1, day1);
		eDate = new GregorianCalendar(year2, month2, day2);
		
		// Istanzia un nuovo oggetto building per aggiungere compagnie finanziatrici è 
		// previsto l'utilizzo dei metodi add o update funded
		b = new Building(n, bTyp, sDate, eDate, null);
		
		// Controlla che il building sia stato correttamente creato
		if(BuildingOperation.create(b) == 1) {
			//System.out.println("Done!");
			return 1;
		}
		else {
			//System.out.println("Something went wrong during creation process, see log for more details");
			return -5;
		}
	}
	
	// Permette di recuperare il cantiere identificato dal parametro passato
	public static Building retrieve(long bId) {
		
		// Legge dal db
		Building b = BuildingOperation.read(bId);
		return b;
	}
	
	// Metodo per eseguire l'update della tipologia del building identificato dal parametro
	// passato
	public static int updateTyp(long bId, String nTyp) {
		
		if(nTyp.equals("")) {
			//System.out.println("Name field is empty, insert a non-empty name");
			return -1;
		}
		
		// Verifica che il building che si intende modificare sia presente nel db
		if(retrieve(bId) == null) {
			//System.out.println("The entity is not in the database");
			return -2;
		}
		
		// Verifica che la modifica sia andata a buon fine
		if(BuildingOperation.updateTypology(bId, nTyp) == 1) {
			//System.out.println("Done!");
			return 1;
		}
		else {
			//System.out.println("Something went wrong during update Typology process, see log for more details");
			return -3;
		}
	}
	
	// Metodo per eseguire l'update della data di inizio lavori del building identificato
	//  dal parametro passato
	public static int updateSDate(long bId, int nYear, int nMonth, int nDay) {
		
		if(nDay <= 0 || nDay >= 32 || nMonth < 0 || nMonth >= 13 || nYear < 0) {
			//System.out.println("The new date has a bad format");
			return -1;
		}
		
		if(retrieve(bId) == null) {
			//System.out.println("The entity is not in the database");
			return -2;
		}
		
		// Verifica che la nuova sia antecedente alla data di fine lavori
		if(retrieve(bId).getEndDate().get(GregorianCalendar.YEAR) < nYear) {
			//System.out.println("New date don't respect relationship with old EndDate);
			return -3;
		}
		
		GregorianCalendar sDate = new GregorianCalendar(nYear, nMonth, nDay);
		if(BuildingOperation.updateSDate(bId, sDate) == 1) {
			//System.out.println("Done!");
			return 1;
		}
		else {
			//System.out.println("Something went wrong during update newDate process, see log for more details");
			return -4;
		}
	}

	// Metodo per eseguire l'update della data di fine lavori del building identificato
	//  dal parametro passato
	public static int updateEDate(long bId, int nYear, int nMonth, int nDay) {
	
		if(nDay <= 0 || nDay >= 32 || nMonth < 0 || nMonth >= 13 || nYear < 0) {
			//System.out.println("The new date has a bad format");
			return -1;
		}
	
		if(retrieve(bId) == null) {
			//System.out.println("The entity is not in the database");
			return -2;
		}
	
		if(retrieve(bId).getStartDate().get(GregorianCalendar.YEAR) > nYear) {
			//System.out.println("New date don't respect relationship with old StartDate);
			return -3;
		}
	
		GregorianCalendar sDate = new GregorianCalendar(nYear, nMonth, nDay);
		if(BuildingOperation.updateEDate(bId, sDate) == 1) {
			//System.out.println("Done!");
			return 1;
		}
		else {
			//System.out.println("Something went wrong during update newDate process, see log for more details");
			return -3;
		}
	}

	// Metodo per eseguire l'update delle aziende finaziatrici del building identificato
	//  dal parametro passato
	public static int updateFunded(long bId, Vector<Company> c) {
		
		if(retrieve(bId) == null) {
			//System.out.println("The entity is not in the database");
			return -1;
		}
		
		if(BuildingOperation.updateFunded(bId, c) == 1) {
			//System.out.println("Done!");
			return 1; 
		}
		else {
			//System.out.println("Something went wrong during update funded process, see log for more details");
			return -2;
		}
	}
	
	// Metodo per eseguire l'aggiunta di anziende finaziatrici del building identificato
	// dal parametro passato
	public static int addFunded(long bId, long cId) {
		
		Vector<Company> c = new Vector<Company>();
		
		if(retrieve(bId) == null) {
			System.out.println("The entity is not in the database");
			return -1;
		}
		
		if(CompanyOperation.read(cId) == null) {
			//System.out.println("The entity is not in the database");
			return -1;
		}
		
		c = (Vector<Company>) BuildingOperation.read(bId).getFunded();
		c.add(CompanyOperation.read(cId));
		
		if(BuildingOperation.updateFunded(bId, c) == 1) {
			//System.out.println("Done!");
			return 1; 
		}
		else {
			//System.out.println("Something went wrong during update funded process, see log for more details");
			return -2;
		}
		
	}
	
	// Metodo per eseguire l'update della location del building identificato
	// dal parametro passato
	public static int updateLocation(long bId, long nId) {
		
		if(retrieve(bId) == null) {
			//System.out.println("The entity is not in the database");
			return -1;
		}
		
		if(BuildingOperation.updateLocation(bId, NeighborhoodOperation.read(nId)) == 1) {
			//System.out.println("Done!");
			return 1; 
		}
		else {
			//System.out.println("Something went wrong during update Location process, see log for more details");
			return -2;
		}
	}
	
	// Metodo per eseguire il remove del building identificato dal parametro passato
	public static int remove(long bId) {
		
		if(retrieve(bId) == null) {
			//System.out.println("The entity is not in the database");
			return -1;
		}
		
		if(BuildingOperation.delete(bId) == 1) {
			//System.out.println("Done!");
			return 1; 
		}
		else {
			//System.out.println("Something went wrong during delete process, see log for more details");
			return -2;
		}
	}
	
	// Operazioni di selezione
	public static List selectAllBuilding() {
		
		return(BuildingOperation.selectAllBuilding());
	}
	
	public static List selectAllBuildingInNeighborhood(long neighId) {
		
		return(BuildingOperation.selectAllBuildingInNeighborhood(neighId));
	}
	
	public static List selectFundedBy(long compId) {
		
		return(BuildingOperation.selectFundedBy(compId));
	}
	
	public static List getBuildStartIn(int year, int month, int day) {
		
		if(day <= 0 || day >= 32 || month < 0 || month >= 13 || year < 0) {
			//System.out.println("The new date has a bad format");
			return null;
		}
	
		GregorianCalendar sDate = new GregorianCalendar(year, month, day);
		return(BuildingOperation.getBuildStartIn(sDate));
		
	}
	
	public static List selectBuilByTypology(String typology) {
		
		if(typology.equals("")) {
			//System.out.println("The typology field is empty, please insert a good typology string");
			return null;
		}
		
		return BuildingOperation.selectByTypology(typology);
	}

}
