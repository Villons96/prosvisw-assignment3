package service;
import java.util.List;

import chara.Company;
import operation.CompanyOperation;
import operation.DatabaseOperation;

// I commenti toccano solo la prima occorrenza dell'istruzione commentata
public class CompanyService {

	// Questo metodo permette di inserire una nuova company
	public static int insert(String cName, String cNationality) {
		
		Company c;
		
		// Controlla che il nome non sia vuoto
		if(cName.equals("")) {
			//System.out.println("Name field is empty, insert a non-empty name");
			return -1;
		}
		
		// Controlla che la nazionalità non sia vuoto
		if(cNationality.equals("")) {
			//System.out.println("Nationality field is empty, insert a non-empty name");
			return -2;
		}
		
		c = new Company(cName, cNationality);
		
		// Controlla che la company sia stata correttamente creata
		if(CompanyOperation.create(c) == 1) {
			//System.out.println("Done!");
			return 1;
		}
		else {
			//System.out.println("Something went wrong during creation process, see log for more details");
			return -3;
		}
	}
	
	// Permette di recuperare la compagnia identificata dal parametro passato
	public static Company retrieve(long cId) {
		
		Company c = CompanyOperation.read(cId);
		return c;
	}
	
	// Metodo per permette l'update del nome, data la compagnia identificata dal parametro
	public static int updateCName(long cId, String nCName) {
		
		if(nCName.equals("")) {
			//System.out.println("Name field is empty, insert a non-empty name");
			return -1;
		}
		
		// Verifica che la compagnia che si intende modificare sia presente nel db
		if(CompanyOperation.read(cId) == null) {
			//System.out.println("The entity is not in the database");
			return -2;
		}
		
		// Verifica che la modifica sia andata a buon fine
		if(CompanyOperation.updateName(cId, nCName) == 1) {
			//System.out.println("Done!");
			return 1;
		}
		else {
			//System.out.println("Something went wrong during update name process, see log for more details");
			return -3;
		}
	}
	
	// Metodo per permette l'update della nazionalità, data la compagnia identificata dal parametro
	public static int updateCNationality(long cId, String nCNationality) {
		
		if(nCNationality.equals("")) {
			//System.out.println("Nationality field is empty, insert a non-empty name");
			return -1;
		}
		
		if(CompanyOperation.read(cId) == null) {
			//System.out.println("The entity is not in the database");
			return -2;
		}
		
		if(CompanyOperation.updateNationality(cId, nCNationality) == 1) {
			//System.out.println("Done!");
			return 1;
		}
		else {
			//System.out.println("Something went wrong during update name process, see log for more details");
			return -3;
		}
	}

	// Metodo per eseguire il remove della company identificata dal parametro passato
	public static int remove(long cId) {
		
		if(CompanyOperation.read(cId) == null) {
			//System.out.println("The entity is not in the database");
			return -1;
		}
		
		if(CompanyOperation.delete(cId) == 1) {
			//System.out.println("Done!");
			return 1;
		}
		else {
			//System.out.println("Something went wrong during delete process, see log for more details");
			return -3;
		}
	}
	
	// Operazioni di selezione
	public static List selectAllCompany() {
		
		return(CompanyOperation.selectAllCompany());
	}
	
	public static List selectCompanyByNationality(String cNationality) {
		
		if(cNationality.equals("")) {
			//System.out.println("Done!");
			return null;
		}
		
		return(CompanyOperation.selectByNationality(cNationality));
	}
	
}
