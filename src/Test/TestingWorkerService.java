package Test;

import static org.junit.Assert.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.junit.After;
import org.junit.Test;
import chara.Worker;
import operation.DatabaseOperation;
import service.WorkerService;

// I commenti toccano solo la prima occorrenza dell'istruzione commentata
public class TestingWorkerService {
	
	// Metodo che viene richiamato automaticamente dopo ogni test, provvede a ripulire il database
	@After 
	public void clean() {
		DatabaseOperation.deleteAllTuple();
	}

	// Inserimento di tre worker senza riferimenti a entità esterne
	@Test
	public void simpleInsertWorker() {
		
		int result = 0;
		
		// Inserisco tre worker
		result += WorkerService.insert("Giacomo", "Villa", 28, 7, 1996, 0, 0);
		result += WorkerService.insert("Anita", "Villa", 22, 6, 1955, 0, 0);
		result += WorkerService.insert("Tommaso", "Villa", 3, 12, 1990, 0, 0);
	
		// Verifico che gli inserimenti abbiano avuto successo
		assertEquals(result, 3);
		
		// Verifico che in caso di nome non valido sia generato un messaggio di errore bloccando
		// dunque il tentativo di inserimento
		assertEquals(WorkerService.insert("", "Villa", 28, 7, 1996, 0, 0), -1);
		
		// Verifico che in caso di cognome non valido sia generato un messaggio di errore bloccando
		// dunque il tentativo di inserimento
		assertEquals(WorkerService.insert("Giacomo", "", 28, 7, 1996, 0, 0), -2);
		
		// Verifico che in caso di data non valida sia generato un messaggio di errore bloccando
		// dunque il tentativo di inserimento
		assertEquals(WorkerService.insert("Giacomo", "Villa", -28, 7, 1996, 0, 0), -3);
		assertEquals(WorkerService.insert("Giacomo", "Villa", 28, -7, 1996, 0, 0), -3);
		assertEquals(WorkerService.insert("Giacomo", "Villa", 28, 7, -1996, 0, 0), -3);
		
		// Verifico che in caso di minore età sia generato un messaggio di errore bloccando
		// dunque il tentativo di inserimento
		assertEquals(WorkerService.insert("Giacomo", "Villa", 28, 7, 2005, 0, 0), -4);
		
		// Verifico che i neigborhood presenti nel db siano veramente tre
		assertEquals(WorkerService.selectAllWorker().size(), 3);
		
	}
	
	// Modifica dei vari parametri di un worker
	@Test
	public void simpleUpdateWorker() {
		
		WorkerService.insert("Giacomo", "Villa", 28, 7, 1996, 0, 0);
		
		// Ottengo la chiave del neigborhood appena iserito
		List<Worker> worker = WorkerService.selectAllWorker();
		long wKey = worker.get(0).getWoId();
		
		// Verifico che la modifica del nome sia eseguita senza problemi
		assertEquals(WorkerService.updateName(wKey, "Gianni"), 1);
		
		// Verifico che in caso di nome non valido sia generato un messaggio di errore bloccando
		// dunque il tentativo di modifica
		assertEquals(WorkerService.updateName(wKey, ""), -1);
		
		// Verifico che in caso di identificativo a un worker non presente sia generato un messaggio di errore 
		// bloccando dunque il tentativo di modifica
		assertEquals(WorkerService.updateName(-1, "Gianni"), -2);
		
		// Verifico che la modifica sia stata effettivamente performata
		assertEquals(WorkerService.retrieve(wKey).getName(), "Gianni");
		
		assertEquals(WorkerService.updateSurname(wKey, "Billa"), 1);
		assertEquals(WorkerService.updateSurname(wKey, ""), -1);
		assertEquals(WorkerService.updateSurname(-1, "Gianni"), -2);
		assertEquals(WorkerService.retrieve(wKey).getSurname(), "Billa");
		
		assertEquals(WorkerService.updatewBDate(wKey, 1993, 8, 5), 1);
		
		// Verifico che in caso di data non valida sia generato un messaggio di errore bloccando 
		// dunque il tentativo di modifica
		assertEquals(WorkerService.updatewBDate(wKey, -1993, 8, 5), -1);
		assertEquals(WorkerService.updatewBDate(wKey, 1993, -8, 5), -1);
		assertEquals(WorkerService.updatewBDate(wKey, 1993, 8, -5), -1);
		assertEquals(WorkerService.updatewBDate(-1, 1993, 8, 5), -2);
		assertEquals(WorkerService.updatewBDate(wKey, 2005, 8, 5), -3);
		assertEquals(WorkerService.retrieve(wKey).getBornDate().get(Calendar.YEAR), 1993);
		assertEquals(WorkerService.retrieve(wKey).getBornDate().get(GregorianCalendar.MONTH), 8);
		assertEquals(WorkerService.retrieve(wKey).getBornDate().get(GregorianCalendar.DAY_OF_MONTH), 5);			
	
	}
	
	// Eliminazione di un worker
	@Test
	public void simpleDeleteWorker() {
		
		WorkerService.insert("Giacomo", "Villa", 28, 7, 1996, 0, 0);
		List<Worker> worker = WorkerService.selectAllWorker();
		long wKey = worker.get(0).getWoId();
		
		// Verifico che l'eliminazione non generi errori 
		assertEquals(WorkerService.remove(wKey), 1);
		
		// Verifico che il tentativo di eliminazione di un worker non presente nel db generi un messaggio
		// di errore bloccando dunque il tentativo
		assertEquals(WorkerService.remove(-1), -1);
		
		// Verifico che l'eliminazione abbia avuto luogo
		assertEquals(WorkerService.retrieve(wKey), null);
		
	}
	
	// Seleziono i worker dati un certo nome e un certo cognome
	@Test 
	public void selectByNameAndSurname() {
		
		WorkerService.insert("Giacomo", "Villa", 28, 7, 1996, 0, 0);
		WorkerService.insert("Giacomo", "Villa", 28, 7, 1996, 0, 0);
		
		WorkerService.insert("Anita", "Villa", 28, 7, 1996, 0, 0);
		
		// Verifico che non ci siano worker con nome vuoto
		assertEquals(WorkerService.selectWorkerByNameAndSurname("", "Villa"), null);
		
		// Verifico che non ci siano worker con cognome vuoto
		assertEquals(WorkerService.selectWorkerByNameAndSurname("Giacomo", ""), null);
		
		// Verifico che ci siano solo due worker con i parametri passati
		assertEquals(WorkerService.selectWorkerByNameAndSurname("Giacomo", "Villa").size(), 2);
		
		// Verifico che ci siano solo un  worker con i parametri passati
		assertEquals(WorkerService.selectWorkerByNameAndSurname("Anita", "Villa").size(), 1);
		
		// Verifico che non ci siano worker con i parametri passati
		assertEquals(WorkerService.selectWorkerByNameAndSurname("Tommaso", "Villa").size(), 0);
		
	}

}
