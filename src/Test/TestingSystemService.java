package Test;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Vector;
import org.junit.After;
import org.junit.Test;
import chara.Building;
import chara.Company;
import chara.Neighborhood;
import chara.Worker;
import operation.WorkerOperation;
import service.BuildingService;
import service.CompanyService;
import service.NeighborhoodService;
import service.WorkerService;

// I commenti toccano solo la prima occorrenza dell'istruzione commentata
public class TestingSystemService {
	
	// Metodo che viene richiamato automaticamente dopo ogni test, provvede a ripulire il database
	@After
	public void clean() {
		
		if(CompanyService.selectAllCompany().size() != 0) {
			List<Company> company = CompanyService.selectAllCompany();
			for(int i = 0; i < company.size(); i++) {
				CompanyService.remove(company.get(i).getCompId());
			}
		}
		
		if(NeighborhoodService.selectAllNeighborhood().size() != 0) {
			List<Neighborhood> neighborhood = NeighborhoodService.selectAllNeighborhood();
			for(int i = 0; i < neighborhood.size(); i++)
				NeighborhoodService.remove(neighborhood.get(i).getNeighId());
		}
		
		if(BuildingService.selectAllBuilding().size() != 0) {
			List<Building> building = BuildingService.selectAllBuilding();
			for(int i = 0; i < building.size(); i++)
				BuildingService.remove(building.get(i).getBuildId());
		}
		
		if(WorkerService.selectAllWorker().size() != 0) {
			List<Worker> worker = WorkerService.selectAllWorker();
			for(int i = 0; i < worker.size(); i++)
				WorkerService.remove(worker.get(i).getWoId());
		}

	}

	// Inserimento di una possibile realtà di interesse
	@Test
	public void hardInsertInSystem() {
		
		List<Neighborhood> neigh; 
		List<Building> build;
		List<Worker> worker;
		List<Company> comp;
			
		// Inserisco tre neighborhood
		NeighborhoodService.insert("Quarto Oggiaro", 2000);
		NeighborhoodService.insert("Bonola", 3000);
		NeighborhoodService.insert("Trenno", 5000);
		
		// Seleziono tutti i neigbhorhood attualmente nel db
		neigh = NeighborhoodService.selectAllNeighborhood();
		
		// Verifico che gli isnerimento abbiano avuto luogo
		assertEquals(neigh.size(), 3);
		
		// Ottengo le chiavi dei neigborhoods appena iseriti
		long nKey1 = neigh.get(0).getNeighId();
		long nKey2 = neigh.get(1).getNeighId();
		long nKey3 = neigh.get(2).getNeighId();
		
		// Inserisco quattro building con riferimenti ai quartieri
		BuildingService.insert(nKey1, "SuperMarket", 1, 1, 2001, 2, 10, 2007);
		BuildingService.insert(nKey2, "Underground", 10, 9, 2005, 12, 3, 2018);
		BuildingService.insert(nKey3, "School", 14, 3, 2009, 6, 1, 2012);
		BuildingService.insert(nKey1, "Office", 5, 9, 2020, 4, 5, 2022);
		build = BuildingService.selectAllBuilding();
		assertEquals(build.size(), 4);
		
		// Verifico che siano stati correttamente registrati i riferiemnti ai quartieri
		assertEquals(BuildingService.selectAllBuildingInNeighborhood(nKey1).size(), 2);
		long bKey1 = build.get(0).getBuildId();
		long bKey2 = build.get(1).getBuildId();
		long bKey3 = build.get(2).getBuildId();
		long bKey4 = build.get(3).getBuildId();
		
		WorkerService.insert("Giacomo", "Villa", 28, 7, 1996, 0, bKey1);
		WorkerService.insert("Theresa", "Mai", 14, 2, 1989, 0, bKey2);
		WorkerService.insert("Gino", "Diajo", 3, 2, 1999, 0, bKey3);
		WorkerService.insert("Frao", "Juer", 6, 11, 1989, 0, bKey4);
		worker = WorkerService.selectAllWorker();
		assertEquals(worker.size(), 4);
		long wKey1 = worker.get(0).getWoId();
		long wKey2 = worker.get(1).getWoId();
		long wKey3 = worker.get(2).getWoId();
		long wKey4 = worker.get(3).getWoId();
		
		// Imposto i vari capi
		WorkerService.updateChief(wKey1, wKey1);
		WorkerService.updateChief(wKey2, wKey2);
		WorkerService.updateChief(wKey3, wKey3);
		WorkerService.updateChief(wKey4, wKey4);
		
		// Inserisco i workers con riferimenti ai capi e ai cantieri
		WorkerService.insert("Ana", "Villa", 22, 6, 1999, wKey1, bKey1);
		WorkerService.insert("Rorta", "Saracino", 14, 11, 1966, wKey1, bKey1);
		WorkerService.insert("Fimena", "Saracino", 3, 8, 1989, wKey1, bKey1);
		
		WorkerService.insert("Gia", "Melna", 8, 10, 1991, wKey2, bKey2);
		WorkerService.insert("Silvo", "Belloni", 1, 1, 1923, wKey2, bKey2);
		
		WorkerService.insert("Jey Berd", "Corri", 2, 2, 1949, wKey3, bKey3);
		WorkerService.insert("Elsa", "Winsurf", 3, 7, 1978, wKey3, bKey3);
		
		worker = WorkerService.selectAllWorker();
		
		// Verifico che i workers siano stati correttamente inserti
		assertEquals(worker.size(), 11);
		
		// Verifico che i riferimenti ai cantieri siano stati correttamente inserti
		assertEquals(WorkerService.selectAllWorkerInBuilding(bKey1).size(), 4);
		
		// Verifico che i riferimenti ai capi siano stati correttamente inserti
		assertEquals(WorkerService.selectAllTeam(wKey1).size(), 4);
		
		CompanyService.insert("AMSA", "Italy");
		CompanyService.insert("ATM", "Italy");
		CompanyService.insert("FIAT", "Italy");
		comp = CompanyService.selectAllCompany();
		assertEquals(comp.size(), 3);
		long cKey1 = comp.get(0).getCompId();
		long cKey2 = comp.get(1).getCompId();
		long cKey3 = comp.get(2).getCompId();
		
		// Aggiungo le compagnie finanziatrici dati i vari building
		BuildingService.addFunded(bKey1, cKey1);
		BuildingService.addFunded(bKey1, cKey2);
		
		BuildingService.addFunded(bKey2, cKey3);
		BuildingService.addFunded(bKey2, cKey1);
		
		BuildingService.addFunded(bKey3, cKey1);
		BuildingService.addFunded(bKey3, cKey2);
		BuildingService.addFunded(bKey3, cKey3);
		
		BuildingService.addFunded(bKey4, cKey1);
		
		// Verifico che i riferimenti alle compagnie finaziatrici abbiano avuto luogo
		assertEquals(BuildingService.selectFundedBy(cKey1).size(), 4);;
		
	}
	
	// Modifica dei posti di lavoro
	@Test
	public void buildingSiteChiefUpdate() {
		
		List<Building> build;
		List<Worker> worker;
		
		BuildingService.insert(0, "SuperMarket", 1, 1, 2001, 2, 10, 2007);
		BuildingService.insert(0, "Underground", 10, 9, 2005, 12, 3, 2018);
		build = BuildingService.selectAllBuilding();
		assertEquals(build.size(), 2);
		long bKey1 = build.get(0).getBuildId();
		long bKey2 = build.get(1).getBuildId();
		
		WorkerService.insert("Giacomo", "Villa", 28, 7, 1996, 0, bKey1);
		worker = WorkerService.selectAllWorker();
		assertEquals(worker.size(), 1);
		long wKey1 = worker.get(0).getWoId();
		WorkerService.updateChief(wKey1, wKey1);
		
		WorkerService.insert("Ana", "Villa", 22, 6, 1999, wKey1, bKey1);
		WorkerService.insert("Rorta", "Saracino", 14, 11, 1966, wKey1, bKey1);
		WorkerService.insert("Fimena", "Saracino", 3, 8, 1989, wKey1, bKey1);
		
		// Verifico che i workers siano tutti in un building
		assertEquals(WorkerOperation.selectAllWorkerInBuilding(bKey1).size(), 4);
		
		// Verifico che la modifica vada a buon fine
		assertEquals(WorkerService.updateBuild(wKey1, bKey2), 1);
		
		// Verifico che la modifica sia stata performata
		assertEquals(WorkerOperation.selectAllWorkerInBuilding(bKey2).size(), 4);
		assertEquals(WorkerOperation.selectAllWorkerInBuilding(bKey1).size(), 0);

	}

	// Aggiungo compagnie finanziatrici
	@Test
	public void addFundedCompany() {
		
		List<Building> build;
		List<Company> comp;
		
		BuildingService.insert(0, "SuperMarket", 1, 1, 2001, 2, 10, 2007);
		build = BuildingService.selectAllBuilding();
		assertEquals(build.size(), 1);
		long bKey = build.get(0).getBuildId();
		
		CompanyService.insert("AMSA", "Italy");
		CompanyService.insert("ATM", "Italy");
		CompanyService.insert("FIAT", "Italy");
		comp = CompanyService.selectAllCompany();
		assertEquals(comp.size(), 3);
		long cKey1 = comp.get(0).getCompId();
		long cKey2 = comp.get(1).getCompId();
		long cKey3 = comp.get(2).getCompId();
		
		// Verifico che l'eventuale aggiunta vada a buon fine
		assertEquals(BuildingService.addFunded(bKey, cKey1), 1);
		assertEquals(BuildingService.addFunded(bKey, cKey2), 1);
		
		comp.clear();
		comp = (List<Company>) BuildingService.retrieve(bKey).getFunded();
		
		// Verifico che le aggiunte siano state correttamente perfomate
		assertEquals(comp.size(), 2);
		Boolean find = false;
		
		// Verifico l'assenza di una certa compagnia
		for(int i = 0; i < comp.size(); i++) 
			if(comp.get(i).getCompId() == cKey3) 
				find = true;
		
		assertEquals(find, false);
		
		assertEquals(BuildingService.addFunded(bKey, cKey3), 1);
		
		comp.clear();
		comp = (List<Company>) BuildingService.retrieve(bKey).getFunded();
		
		// Verifico la presenza di una certa compagnia
		for(int i = 0; i < comp.size(); i++) 
			if(comp.get(i).getCompId() == cKey3) 
				find = true;
		
		assertEquals(find, true);
		assertEquals(comp.size(), 3);
		
		// Verifico che l'aggiunta di una compagnia già presente generi un errore bloccando dunque
		// l'aggiuta
		assertEquals(BuildingService.addFunded(bKey, cKey3), -2);
		
	}
	
	// Modifico le compagnie finanziatrici
	@Test
	public void updateFundedCompany() {
		
		List<Building> build;
		List<Company> comp;
		Vector<Company> newComp = new Vector<Company>();
		
		BuildingService.insert(0, "SuperMarket", 1, 1, 2001, 2, 10, 2007);
		build = BuildingService.selectAllBuilding();
		assertEquals(build.size(), 1);
		long bKey = build.get(0).getBuildId();
		
		CompanyService.insert("AMSA", "Italy");
		CompanyService.insert("ATM", "Italy");
		CompanyService.insert("FIAT", "Italy");
		comp = CompanyService.selectAllCompany();
		assertEquals(comp.size(), 3);
		long cKey1 = comp.get(0).getCompId();
		long cKey2 = comp.get(1).getCompId();
		long cKey3 = comp.get(2).getCompId();
		
		// Aggiungo una prima compagnia e verifico che questa sia stata aggiunta
		assertEquals(BuildingService.addFunded(bKey, cKey1), 1);
		assertEquals(BuildingService.retrieve(bKey).getFunded().size(), 1);
		
		// Aggiungo a un vector le nuove compagnie 
		newComp.add(CompanyService.retrieve(cKey2));
		newComp.add(CompanyService.retrieve(cKey3));
		
		// Modifico le compagnia finanziatrici 
		assertEquals(BuildingService.updateFunded(bKey, newComp), 1);
		
		// Verifico che la modifica abbia avuto luogo
		assertEquals(BuildingService.retrieve(bKey).getFunded().size(), 2);
		
		// Verifico che la compagnia precedente non sia più una compagnia finaziatrice
		assertEquals(BuildingService.retrieve(bKey).getFunded().contains(CompanyService.retrieve(cKey1)), false);
			
	}
	
	// Elimino workers e chief
	@Test
	public void deleteWorkerInSystem() {
		
		List<Worker> worker;
		
		WorkerService.insert("Giacomo", "Villa", 28, 7, 1996, 0, 0);
		WorkerService.insert("Theresa", "Mai", 14, 2, 1989, 0, 0);
		WorkerService.insert("Gino", "Diajo", 3, 2, 1999, 0, 0);
		worker = WorkerService.selectAllWorker();
		long wKey1 = worker.get(0).getWoId();
		long wKey2 = worker.get(1).getWoId();
		long wKey3 = worker.get(2).getWoId();
		WorkerService.updateChief(wKey1, wKey1);
		WorkerService.updateChief(wKey2, wKey2);
		WorkerService.updateChief(wKey3, wKey3);
		
		WorkerService.insert("Ana", "Villa", 22, 6, 1999, wKey1, 0);
		WorkerService.insert("Rorta", "Saracino", 14, 11, 1966, wKey1, 0);
		WorkerService.insert("Fimena", "Saracino", 3, 8, 1989, wKey1, 0);
		
		WorkerService.insert("Gia", "Melna", 8, 10, 1991, wKey2, 0);
		WorkerService.insert("Silvo", "Belloni", 1, 1, 1923, wKey2, 0);
		
		WorkerService.insert("Jey Berd", "Corri", 2, 2, 1949, wKey3, 0);
		WorkerService.insert("Elsa", "Winsurf", 3, 7, 1978, wKey3, 0);
		
		worker.clear();
		worker = WorkerService.selectAllWorker();
		assertEquals(WorkerService.remove(worker.get(9).getWoId()), 1);
		
		// Verifico che l'eliminazione non generi errori
		assertEquals(WorkerService.remove(wKey1), 1);
		
		// Verifico che l'eliminazione abbia avuto effettivamente luogo
		assertEquals(WorkerService.selectAllTeam(wKey1).size(), 0);
		
		// Verifico che l'eliminazione di un worker non presente generi un errore e dunque che 
		// il tentativo sia bloccato
		assertEquals(WorkerService.remove(wKey1), -1);
		
		// Elimino i restanti capi
		WorkerService.remove(wKey2);
		WorkerService.remove(wKey3);
		
		// Verifico che le eliminazioni a castata siano state performate
		assertEquals(WorkerService.selectAllWorker().size(), 0);	
	}
	
	// Elimino building
	@Test
	public void deleteBuildingInSystem() {
		
		List<Worker> worker;
		List<Building> build;
		
		BuildingService.insert(0, "SuperMarket", 1, 1, 2001, 2, 10, 2007);
		BuildingService.insert(0, "Underground", 10, 9, 2005, 12, 3, 2018);
		BuildingService.insert(0, "School", 14, 3, 2009, 6, 1, 2012);
		build = BuildingService.selectAllBuilding();
		long bKey1 = build.get(0).getBuildId();
		long bKey2 = build.get(1).getBuildId();
		long bKey3 = build.get(2).getBuildId();
		
		WorkerService.insert("Giacomo", "Villa", 28, 7, 1996, 0, bKey1);
		WorkerService.insert("Theresa", "Mai", 14, 2, 1989, 0, bKey2);
		WorkerService.insert("Gino", "Diajo", 3, 2, 1999, 0, bKey3);
		worker = WorkerService.selectAllWorker();
		long wKey1 = worker.get(0).getWoId();
		long wKey2 = worker.get(1).getWoId();
		long wKey3 = worker.get(2).getWoId();
		WorkerService.updateChief(wKey1, wKey1);
		WorkerService.updateChief(wKey2, wKey2);
		WorkerService.updateChief(wKey3, wKey3);
		
		WorkerService.insert("Ana", "Villa", 22, 6, 1999, wKey1, bKey1);
		WorkerService.insert("Rorta", "Saracino", 14, 11, 1966, wKey1, bKey1);
		WorkerService.insert("Fimena", "Saracino", 3, 8, 1989, wKey1, bKey1);
		
		WorkerService.insert("Gia", "Melna", 8, 10, 1991, wKey2, bKey2);
		WorkerService.insert("Silvo", "Belloni", 1, 1, 1923, wKey2, bKey2);
		
		WorkerService.insert("Jey Berd", "Corri", 2, 2, 1949, wKey3, bKey3);
		WorkerService.insert("Elsa", "Winsurf", 3, 7, 1978, wKey3, bKey3);
		
		// Verifico che l'eliminazione non generi errori
		assertEquals(BuildingService.remove(bKey1), 1);
		
		// Verifico che l'eliminazione a cascata abbia avuto luogo 
		assertEquals(WorkerService.selectAllWorkerInBuilding(bKey1).size(), 0);
		
		// Verifico che l'eliminazione di un building non presente generi un errore e dunque che 
		// il tentativo sia bloccato 
		assertEquals(BuildingService.remove(bKey1), -1);
		
		assertEquals(WorkerService.selectAllWorkerInBuilding(bKey2).size(), 3);
		
		// Elimino i restanti building
		BuildingService.remove(bKey2);
		BuildingService.remove(bKey3);
		
		// Verifico che l'eliminazione in cascata abbia avuto luogo
		assertEquals(WorkerService.selectAllWorker().size(), 0);	
		assertEquals(BuildingService.selectAllBuilding().size(), 0);
	} 
	
	// Eliminazione dei neigborhood
	@Test
	public void deleteNeighborhoodInSystem() {
		
		List<Worker> worker;
		List<Building> build;
		List<Neighborhood> neigh; 
		
		NeighborhoodService.insert("Quarto Oggiaro", 2000);
		NeighborhoodService.insert("Bonola", 3000);
		NeighborhoodService.insert("Trenno", 5000);
		neigh = NeighborhoodService.selectAllNeighborhood();
		long nKey1 = neigh.get(0).getNeighId();
		long nKey2 = neigh.get(1).getNeighId();
		long nKey3 = neigh.get(2).getNeighId();
		
		BuildingService.insert(nKey1, "SuperMarket", 1, 1, 2001, 2, 10, 2007);
		BuildingService.insert(nKey2, "Underground", 10, 9, 2005, 12, 3, 2018);
		BuildingService.insert(nKey3, "School", 14, 3, 2009, 6, 1, 2012);
		BuildingService.insert(nKey1, "Office", 5, 9, 2020, 4, 5, 2022);
		build = BuildingService.selectAllBuilding();
		long bKey1 = build.get(0).getBuildId();
		long bKey2 = build.get(1).getBuildId();
		long bKey3 = build.get(2).getBuildId();
		
		WorkerService.insert("Giacomo", "Villa", 28, 7, 1996, 0, bKey1);
		WorkerService.insert("Theresa", "Mai", 14, 2, 1989, 0, bKey2);
		WorkerService.insert("Gino", "Diajo", 3, 2, 1999, 0, bKey3);
		worker = WorkerService.selectAllWorker();
		long wKey1 = worker.get(0).getWoId();
		long wKey2 = worker.get(1).getWoId();
		long wKey3 = worker.get(2).getWoId();
		WorkerService.updateChief(wKey1, wKey1);
		WorkerService.updateChief(wKey2, wKey2);
		WorkerService.updateChief(wKey3, wKey3);
		
		WorkerService.insert("Ana", "Villa", 22, 6, 1999, wKey1, bKey1);
		WorkerService.insert("Rorta", "Saracino", 14, 11, 1966, wKey1, bKey1);
		WorkerService.insert("Fimena", "Saracino", 3, 8, 1989, wKey1, bKey1);
		
		WorkerService.insert("Gia", "Melna", 8, 10, 1991, wKey2, bKey2);
		WorkerService.insert("Silvo", "Belloni", 1, 1, 1923, wKey2, bKey2);
		
		WorkerService.insert("Jey Berd", "Corri", 2, 2, 1949, wKey3, bKey3);
		WorkerService.insert("Elsa", "Winsurf", 3, 7, 1978, wKey3, bKey3);
		
		// Verifico che l'eliminazione non generi errori 
		assertEquals(NeighborhoodService.remove(nKey1), 1);
		
		// Verifico che l'eliminazione a casta del building non abbia generato errori
		assertEquals(BuildingService.selectAllBuildingInNeighborhood(nKey1).size(), 0);
		
		// Verifico che l'eliminazione di un neigbhorhood non presente generi un errore e dunque che 
		// il tentativo sia bloccato
		assertEquals(NeighborhoodService.remove(nKey1), -1);
		
		// Verifico che l'eliminazione dei building abbia correttamente generato l'eliminazine dei worker
		assertEquals(WorkerService.selectAllWorker().size(), 6);
		
		// Elimino i restanti neigborhood
		NeighborhoodService.remove(nKey2);
		NeighborhoodService.remove(nKey3);
		
		// Verifico che le eliminazioni a cascata abbiano avuto luogo
		assertEquals(NeighborhoodService.selectAllNeighborhood().size(), 0);
		assertEquals(BuildingService.selectAllBuilding().size(), 0);
		assertEquals(WorkerService.selectAllWorker().size(), 0);
	}
	
	// Elimino le compagny
	@Test
	public void deleteCompanyInSystem() {
		
		List<Building> build;
		List<Company> comp;
			
		BuildingService.insert(0, "SuperMarket", 1, 1, 2001, 2, 10, 2007);
		BuildingService.insert(0, "Underground", 10, 9, 2005, 12, 3, 2018);
		BuildingService.insert(0, "School", 14, 3, 2009, 6, 1, 2012);
		BuildingService.insert(0, "Office", 5, 9, 2020, 4, 5, 2022);
		build = BuildingService.selectAllBuilding();
		long bKey1 = build.get(0).getBuildId();
		long bKey2 = build.get(1).getBuildId();
		long bKey3 = build.get(2).getBuildId();
		
		CompanyService.insert("AMSA", "Italy");
		CompanyService.insert("ATM", "Italy");
		CompanyService.insert("FIAT", "Italy");
		comp = CompanyService.selectAllCompany();
		long cKey1 = comp.get(0).getCompId();
		long cKey2 = comp.get(1).getCompId();
		long cKey3 = comp.get(2).getCompId();
		
		BuildingService.addFunded(bKey1, cKey1);
		BuildingService.addFunded(bKey1, cKey2);
		
		BuildingService.addFunded(bKey2, cKey2);
		BuildingService.addFunded(bKey2, cKey1);
		
		BuildingService.addFunded(bKey3, cKey3);
		
		// Verifico che l'eliminazione di una company non generi erroi
		assertEquals(CompanyService.remove(cKey1), 1);
		
		// Verifico che l'eliminazione abbia avuto luogo 
		assertEquals(CompanyService.selectAllCompany().size(), 2);
		assertEquals(BuildingService.retrieve(bKey1).getFunded().size(), 1);
		
		// Verifico che l'eliminazione di una company non presente generi un errore e dunque che 
		// il tentativo sia bloccato
		assertEquals(CompanyService.remove(cKey1), -1);
		
		CompanyService.remove(cKey2);
		
		// Verifico che l'eliminazione delle compagnie finanziatrici causi l'eliminazioen del cantiere
		assertEquals(BuildingService.retrieve(bKey2), null);
		
		CompanyService.remove(cKey3);
		
		// Verifico che le eliminazioni abbiano avuto luogo
		assertEquals(BuildingService.selectAllBuilding().size(), 0);
		assertEquals(CompanyService.selectAllCompany().size(), 0);
		
	}

	// Seleziono i cantieri finanziati
	@Test
	public void getAllFundedBy() {
		
		List<Building> build;
		List<Company> comp;
			
		BuildingService.insert(0, "SuperMarket", 1, 1, 2001, 2, 10, 2007);
		BuildingService.insert(0, "Underground", 10, 9, 2005, 12, 3, 2018);
		BuildingService.insert(0, "School", 14, 3, 2009, 6, 1, 2012);
		build = BuildingService.selectAllBuilding();
		long bKey1 = build.get(0).getBuildId();
		long bKey2 = build.get(1).getBuildId();
		long bKey3 = build.get(2).getBuildId();
		
		CompanyService.insert("AMSA", "Italy");
		CompanyService.insert("ATM", "Italy");
		CompanyService.insert("FIAT", "Italy");
		comp = CompanyService.selectAllCompany();
		long cKey1 = comp.get(0).getCompId();
		long cKey2 = comp.get(1).getCompId();
		long cKey3 = comp.get(2).getCompId();
		
		BuildingService.addFunded(bKey1, cKey1);
		BuildingService.addFunded(bKey1, cKey3);
		
		BuildingService.addFunded(bKey2, cKey2);
		BuildingService.addFunded(bKey2, cKey3);
		
		BuildingService.addFunded(bKey3, cKey3);
		
		// Verifico che i cantieri finaziati siano quelli che mi aspetto
		assertEquals(BuildingService.selectFundedBy(cKey3).size(), 3);
		assertEquals(BuildingService.selectFundedBy(cKey2).size(), 1);
		assertEquals(BuildingService.selectFundedBy(cKey1).size(), 1);
		
	}
			
}
