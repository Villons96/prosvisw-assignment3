package Test;

import static org.junit.Assert.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.junit.After;
import org.junit.Test;
import chara.Building;
import operation.DatabaseOperation;
import service.BuildingService;

// I commenti toccano solo la prima occorrenza dell'istruzione commentata
public class TestingBuildingService {
	
	// Metodo che viene richiamato automaticamente dopo ogni test, provvede a ripulire il database
	@After 
	public void clean() {
		DatabaseOperation.deleteAllTuple();
	}

	// Inserimento di tre building senza riferimenti a entità esterne
	@Test
	public void simpleInsertBuilding() {
		
		int result = 0;
		
		// Inserisco tre building
		result += BuildingService.insert(0, "Underground", 22, 11, 2022, 23, 2, 2023);
		result += BuildingService.insert(0, "School", 24, 0, 2020, 12, 4, 2025);
		result += BuildingService.insert(0, "Office", 4, 11, 2022, 23, 2, 2023);
		
		// Verifico che gli inserimenti abbiano avuto successo
		assertEquals(result, 3);
		
		// Verifico che in caso di typology non valida sia generato un messaggio di errore bloccando
		// dunque il tentativo di inserimento
		assertEquals(BuildingService.insert(0, "", 22, 12, 2022, 23, 2, 2023), -1);
		
		// Verifico che in caso di data di inizio non valida sia generato un messaggio di errore bloccando
		// dunque il tentativo di inserimento
		assertEquals(BuildingService.insert(0, "Underground", -4, 12, 2022, 23, 2, 2023), -2);	
		assertEquals(BuildingService.insert(0, "Underground", 4, -12, 2022, 23, 2, 2023), -2);
		assertEquals(BuildingService.insert(0, "Underground", 4, 12, -2022, 23, 2, 2023), -2);
		
		// Verifico che in caso di data di fine non valida sia generato un messaggio di errore bloccando
		// dunque il tentativo di inserimento
		assertEquals(BuildingService.insert(0, "Underground", 4, 12, 2022, -23, 2, 2023), -3);	
		assertEquals(BuildingService.insert(0, "Underground", 4, 12, 2022, 23, -2, 2023), -3);
		assertEquals(BuildingService.insert(0, "Underground", 4, 12, 2022, 23, 2, -2023), -3);
		
		// Verifico che in caso di data fine minore di data fine sia generato un errore bloccando 
		// dunque il tentativo di inserimento
		assertEquals(BuildingService.insert(0, "Underground", 4, 12, 2022, 23, 2, 2000), -4);
		
		// Verifico che i building presenti nel db siano veramente tre
		assertEquals(BuildingService.selectAllBuilding().size(), 3);
		
	}
	
	// Modifica dei vari parametri di un building
	@Test
	public void simpleUpdateBuilding() {
		
		BuildingService.insert(0, "Underground", 22, 11, 2022, 23, 2, 2023);
		
		// Ottengo la chiave del building appena iserito
		List<Building> build = BuildingService.selectAllBuilding();
		long bKey = build.get(0).getBuildId();
		
		// Verifico che la modifica della typology sia eseguita senza problemi
		assertEquals(BuildingService.updateTyp(bKey, "SuperMarket"), 1);
		
		// Verifico che in caso di typology non valida sia generato un messaggio di errore bloccando
		// dunque il tentativo di modifica
		assertEquals(BuildingService.updateTyp(bKey, ""), -1);
		
		// Verifico che in caso di identificativo a un building non presente sia generato un messaggio di errore 
		// bloccando dunque il tentativo di modifica
		assertEquals(BuildingService.updateTyp(-1, "Underground"), -2);
		
		// Verifico che la modifica sia stata effettivamente performata
		assertEquals(BuildingService.retrieve(bKey).getTypology(), "SuperMarket");
		
		// Verifico che la modifica della data di inizio sia eseguita senza problemi
		assertEquals(BuildingService.updateSDate(bKey, 2021, 0, 10), 1);
		
		// Verifico che in caso di data non valida sia generato un messaggio di errore bloccando 
		// dunque il tentativo di modifica
		assertEquals(BuildingService.updateSDate(bKey, -2021, 10, 10), -1);
		assertEquals(BuildingService.updateSDate(bKey, 2021, -10, 10), -1);
		assertEquals(BuildingService.updateSDate(bKey, 2021, 10, -10), -1);
		assertEquals(BuildingService.updateSDate(-1, 2021, 10, 10), -2);
		
		// Verifico che in caso di data di inizio lavori successiva alla data di fine sia generato un messaggio
		// di errore bloccando dunque il tentativo di modifica
		assertEquals(BuildingService.updateSDate(bKey, 2050, 10, 10), -3);
		
		// Verifico che le modifiche abbiano avuto luogo 
		assertEquals(BuildingService.retrieve(bKey).getStartDate().get(GregorianCalendar.YEAR), 2021);
		assertEquals(BuildingService.retrieve(bKey).getStartDate().get(GregorianCalendar.MONTH), 0);
		assertEquals(BuildingService.retrieve(bKey).getStartDate().get(GregorianCalendar.DAY_OF_MONTH) , 10);
		
		assertEquals(BuildingService.updateEDate(bKey, 2027, 5, 13), 1);
		assertEquals(BuildingService.updateEDate(bKey, -2021, 10, 10), -1);
		assertEquals(BuildingService.updateEDate(bKey, 2021, -10, 10), -1);
		assertEquals(BuildingService.updateEDate(bKey, 2021, 10, -10), -1);
		assertEquals(BuildingService.updateEDate(-1, 2021, 10, 10), -2);
		assertEquals(BuildingService.updateEDate(bKey, 1990, 10, 10), -3);
		assertEquals(BuildingService.retrieve(bKey).getEndDate().get(Calendar.YEAR), 2027);
		assertEquals(BuildingService.retrieve(bKey).getEndDate().get(GregorianCalendar.MONTH), 5);
		assertEquals(BuildingService.retrieve(bKey).getEndDate().get(GregorianCalendar.DAY_OF_MONTH), 13);
		
	}
	
	// Eliminazione di un building
	@Test
	public void simpleDeleteBuilding() {
		
		BuildingService.insert(0, "Underground", 22, 11, 2022, 23, 2, 2023);
		List<Building> build = BuildingService.selectAllBuilding();
		long bKey = build.get(0).getBuildId();
		
		// Verifico che l'eliminazione non generi errori 
		assertEquals(BuildingService.remove(bKey), 1);
		
		// Verifico che il tentativo di eliminazione di un building non presente nel db generi un messaggio
		// di errore bloccando dunque il tentativo
		assertEquals(BuildingService.remove(bKey), -1);
		
		// Verifico che l'eliminazione abbia avuto luogo
		assertEquals(BuildingService.retrieve(bKey), null);
		
	}
	
	// Selezione di un quartiere in funzione della tipologia
	@Test
	public void selectNeighByTypology() {
		
		BuildingService.insert(0, "Underground", 22, 11, 2022, 23, 2, 2023);
		BuildingService.insert(0, "Underground", 22, 11, 2022, 23, 2, 2023);
		
		BuildingService.insert(0, "Office", 4, 11, 2022, 23, 2, 2023);
		
		// Verifico che non ci siano building con typology vuota
		assertEquals(BuildingService.selectBuilByTypology(""), null);
		
		// Verifico che ci siano solo due building con typlogy pari a "Underground"
		assertEquals(BuildingService.selectBuilByTypology("Underground").size(), 2);
		
		// Verifico che ci siano solo un building con typlogy pari a "Office"
		assertEquals(BuildingService.selectBuilByTypology("Office").size(), 1);
		
		// Verifico che non ci siano building con typlogy pari a "School"
		assertEquals(BuildingService.selectBuilByTypology("School").size(), 0);
		
	}
	
	// Seleziono building in funzione della data
	@Test
	public void getBuildStartIn() {
		
		List<Building> build;
		
		BuildingService.insert(0, "SuperMarket", 1, 1, 2001, 2, 10, 2007);
		BuildingService.insert(0, "Underground", 10, 9, 2005, 12, 3, 2018);
		BuildingService.insert(0, "School", 14, 3, 2009, 6, 1, 2012);
		
		// Verico che la query restituisca un risultato consono in funzione delle date passate
		assertEquals(BuildingService.getBuildStartIn(2000, 1, 1).size(), 3);
		assertEquals(BuildingService.getBuildStartIn(2005, 1, 1).size(), 2);
		assertEquals(BuildingService.getBuildStartIn(2020, 1, 1).size(), 0);
		
	}

}
