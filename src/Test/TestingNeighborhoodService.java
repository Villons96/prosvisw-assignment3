package Test;

import static org.junit.Assert.*;
import java.util.List;
import org.junit.After;
import org.junit.Test;
import chara.Neighborhood;
import operation.DatabaseOperation;
import service.NeighborhoodService;

// I commenti toccano solo la prima occorrenza dell'istruzione commentata
public class TestingNeighborhoodService {
	
	// Metodo che viene richiamato automaticamente dopo ogni test, provvede a ripulire il database
	@After 
	public void clean() {
		DatabaseOperation.deleteAllTuple();
	}

	// Inserimento di tre neigbhorhood senza riferimenti a entità esterne
	@Test
	public void simpleInsertNeighborhood() {
		
		int result = 0;
		
		// Inserisco tre neighborhood
		result += NeighborhoodService.insert("Quarto Cagnino", 1000);
		result += NeighborhoodService.insert("Baggio", 500);
		result += NeighborhoodService.insert("Bonola", 2000);
		
		// Verifico che gli inserimenti abbiano avuto successo
		assertEquals(result, 3);
		
		// Verifico che in caso di nome non valido sia generato un messaggio di errore bloccando
		// dunque il tentativo di inserimento
		assertEquals(NeighborhoodService.insert("", 1000), -1);
		
		// Verifico che in caso di popolazione non valida sia generato un messaggio di errore bloccando
		// dunque il tentativo di inserimento
		assertEquals(NeighborhoodService.insert("Pt. Genova", -10), -2);
		
		// Verifico che i neigborhood presenti nel db siano veramente tre
		assertEquals(NeighborhoodService.selectAllNeighborhood().size(), 3);
		
	}

	// Modifica dei vari parametri di un neigborhood
	@Test
	public void simpleUpdateNeighborhood() {
		
		NeighborhoodService.insert("Quarto Cagnino", 1000);
		
		// Ottengo la chiave del neigborhood appena iserito
		List<Neighborhood> neigh = NeighborhoodService.selectAllNeighborhood();
		long nKey = neigh.get(0).getNeighId();
		
		// Verifico che la modifica del nome sia eseguita senza problemi
		assertEquals(NeighborhoodService.updateName(nKey, "Trenno"), 1);
		
		// Verifico che in caso di nome non valido sia generato un messaggio di errore bloccando
		// dunque il tentativo di modifica
		assertEquals(NeighborhoodService.updateName(nKey, ""), -1);
		
		// Verifico che in caso di identificativo a un neigborhood non presente sia generato un messaggio di errore 
		// bloccando dunque il tentativo di modifica
		assertEquals(NeighborhoodService.updateName(-1, "Trenno"), -2);
		
		// Verifico che la modifica sia stata effettivamente performata
		assertEquals(NeighborhoodService.retrieve(nKey).getNeighName(), "Trenno");

		assertEquals(NeighborhoodService.updatepopulation(nKey, 100), 1);
		assertEquals(NeighborhoodService.updatepopulation(nKey, -10), -1);
		assertEquals(NeighborhoodService.updatepopulation(-1, 10000), -2);
		assertEquals(NeighborhoodService.retrieve(nKey).getPopulation(), 100);
		
	}
	
	// Eliminazione di una neigborhood
	@Test
	public void simpleDeleteNeighborhood() {
		
		NeighborhoodService.insert("Quarto Cagnino", 1000);
		List<Neighborhood> neigh = NeighborhoodService.selectAllNeighborhood();
		long nKey = neigh.get(0).getNeighId();
		
		// Verifico che l'eliminazione non generi errori 
		assertEquals(NeighborhoodService.remove(nKey), 1);
		
		// Verifico che il tentativo di eliminazione di un neigbhorhood non presente nel db generi un messaggio
		// di errore bloccando dunque il tentativo
		assertEquals(NeighborhoodService.remove(-1), -1);
		
		// Verifico che l'eliminazione abbia avuto luogo
		assertEquals(NeighborhoodService.retrieve(nKey), null);
		
	}
	
	// Selezione di un neigborhood dato il nome
	@Test
	public void selectNeighByName() {
		
		NeighborhoodService.insert("Quarto Cagnino", 1000);
		NeighborhoodService.insert("Baggio", 500);
		List<Neighborhood> neigh = NeighborhoodService.selectAllNeighborhood();
		long nKey1 = neigh.get(0).getNeighId();
		long nKey2 = neigh.get(1).getNeighId();
		
		// Verifico che non ci siano neigborhood con nome vuoto
		assertEquals(NeighborhoodService.selectNeighborhoodByName(""), null);
		
		// Verifico che sia presente un quartiere con il nome passato
		assertEquals(NeighborhoodService.selectNeighborhoodByName("Quarto Cagnino").getNeighId(), nKey1);
		assertEquals(NeighborhoodService.selectNeighborhoodByName("Baggio").getNeighId(), nKey2);
		
		// Verifico che non ci sia il quartiere con il nome passato
		assertEquals(NeighborhoodService.selectNeighborhoodByName("Bonola"), null);
		
	}

}
