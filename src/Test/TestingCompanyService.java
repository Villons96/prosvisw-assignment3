package Test;

import static org.junit.Assert.*;
import java.util.List;
import org.junit.After;
import org.junit.Test;
import chara.Company;
import operation.DatabaseOperation;
import service.CompanyService;

// I commenti toccano solo la prima occorrenza dell'istruzione commentata
public class TestingCompanyService {
	
	// Metodo che viene richiamato automaticamente dopo ogni test, provvede a ripulire il database
	@After 
	public void clean() {
		DatabaseOperation.deleteAllTuple();
	}

	// Inserimento di tre company senza riferimenti a entità esterne
	@Test
	public void simpleInsertCompany() {
		
		int result = 0;
		
		// Inserisco tre company
		result += CompanyService.insert("Amsa", "Italy");
		result += CompanyService.insert("ATM", "Italy");
		result += CompanyService.insert("Deutsche Bank", "Germany");
		
		// Verifico che gli inserimenti abbiano avuto successo
		assertEquals(result, 3);
		
		// Verifico che in caso di nome non valido sia generato un messaggio di errore bloccando
		// dunque il tentativo di inserimento
		assertEquals(CompanyService.insert("", "Italy"), -1);
		
		// Verifico che in caso di nazionalità non valida sia generato un messaggio di errore bloccando
		// dunque il tentativo di inserimento
		assertEquals(CompanyService.insert("Amsa", ""), -2);
		
		// Verifico che le company presenti nel db siano veramente tre
		assertEquals(CompanyService.selectAllCompany().size(), 3);
		
	}
	
	// Modifica dei vari parametri di una company
	@Test
	public void simpleUpdateCompany() {
		
		CompanyService.insert("Amsa", "Italy");
		
		// Ottengo la chiave della company appena iserita
		List<Company> comp = CompanyService.selectAllCompany();
		long cKey = comp.get(0).getCompId();
		
		// Verifico che la modifica del nome sia eseguita senza problemi
		assertEquals(CompanyService.updateCName(cKey, "Trasporti Milanesi"), 1);
		
		// Verifico che in caso di nome non valido sia generato un messaggio di errore bloccando
		// dunque il tentativo di modifica
		assertEquals(CompanyService.updateCName(cKey, ""), -1);
		
		// Verifico che in caso di identificativo a una company non presente sia generato un messaggio di errore 
		// bloccando dunque il tentativo di modifica
		assertEquals(CompanyService.updateCName(-1, "Trasporti Milanesi"), -2);
		
		// Verifico che la modifica sia stata effettivamente performata
		assertEquals(CompanyService.retrieve(cKey).getCompName(), "Trasporti Milanesi");
		
		assertEquals(CompanyService.updateCNationality(cKey, "French"), 1);
		assertEquals(CompanyService.updateCNationality(cKey, ""), -1);
		assertEquals(CompanyService.updateCNationality(-1, "French") , -2);
		assertEquals(CompanyService.retrieve(cKey).getCompNationality(), "French");
		
	}
	
	// Eliminazione di una company
	@Test
	public void simpleRemoveCompany() {
		
		CompanyService.insert("Amsa", "Italy");
		List<Company> comp = CompanyService.selectAllCompany();
		long cKey = comp.get(0).getCompId();
		
		// Verifico che l'eliminazione non generi errori 
		assertEquals(CompanyService.remove(cKey), 1);
		
		// Verifico che il tentativo di eliminazione di una company non presente nel db generi un messaggio
		// di errore bloccando dunque il tentativo
		assertEquals(CompanyService.remove(-1), -1);
		
		// Verifico che l'eliminazione abbia avuto luogo
		assertEquals(CompanyService.retrieve(cKey), null);
		
	}
	
	// Selezione di una company in funzione della nazionalità
	@Test
	public void selectByNationality() {
		
		CompanyService.insert("Amsa", "Italy");
		CompanyService.insert("Amsa", "Italy");
		CompanyService.insert("Amsa", "Italy");
		
		CompanyService.insert("Amsa", "French");
		CompanyService.insert("Amsa", "French");

		// Verifico che non ci siano company con nazionalità vuota
		assertEquals(CompanyService.selectCompanyByNationality(""), null);
		
		// Verifico che ci siano solo tre company con nazionalità italiana
		assertEquals(CompanyService.selectCompanyByNationality("Italy").size(), 3);
		
		// Verifico che ci siano solo due company con nazionalità francese
		assertEquals(CompanyService.selectCompanyByNationality("French").size(), 2);
		
		// Verifico che ci sia solo una company con nazionalità americana
		assertEquals(CompanyService.selectCompanyByNationality("USA").size(), 0);
		
	}

}



