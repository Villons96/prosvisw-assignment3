//Questa classe rappresenta l'entità Worker della realtà di interesse
package chara;
import javax.persistence.*;

import java.util.Calendar;

// Definisco il fatto che tale classe è un'entità
@Entity 
// Specifico che la generazione e autoincremento della chiave primaria
@SequenceGenerator(name="Worker_Sequence",sequenceName="W_S")
// Ridefinisco il nome della tabella all'interno del DB
@Table(name = "Worker")
public class Worker{

	// Definisco l'attributo che rappresenta la chiave primaria e li cambio il nome
	@Id @Column(name="\"Id\"")
	@GeneratedValue (strategy=GenerationType.SEQUENCE, generator="Worker_Sequence")
	private long woId;
	
	// Definisco l'attributo che rappresenta la chiave esterna per identificare il 
	// il capo del worker 
	@ManyToOne(targetEntity=Worker.class) @JoinColumn(name = "chief")
	private Worker chief;
	
	// Definisco l'attributo che rappresenta la data di nascita
	// Utilizzo @Temporal per definire che si tratta di una data 
	// e li cambio il nome
	@Temporal(TemporalType.DATE)
	@Column(name="\"bornDate\"")
	private Calendar bornDate;
	
	// Definisco l'attributo che rappresenta la chiave esterna per identificare dove
	// il worker lavora
	@ManyToOne(targetEntity=Building.class) @JoinColumn(name = "buildingSite")
	private Building buildingSite;
	
	// Definisco l'attributo che rappresenta il nome del lavoratore e li cambio nome
	@Column(name="\"Name\"")
	private String name;
	
	// Definisco l'attributo che rappresenta il cognome del lavoratore e li cambio nome
	@Column(name="\"Surname\"")
	private String surname;
	
	// Costruttore di default
	public Worker() {
		
	}
	
	// Costruttore alternativo
	public Worker(String nameW, String surnameW, Calendar bornW, Worker chiefW, Building buildingW) {
		this.setName(nameW);
		this.setSurname(surnameW);
		this.setBornDate(bornW);
		this.setChief(chiefW);
		this.setBuildingSite(buildingW);
	}
	
	// Metodi getters, setters e toString 
	public long getWoId() {
		return woId;
	}
	public void setWoId(long woId) {
		this.woId = woId;
	}
	
	public Worker getChief() {
		return chief;
	}
	public void setChief(Worker chief) {
		this.chief = chief;
	}
	
	public Building getBuildingSite() {
		return buildingSite;
	}
	public void setBuildingSite(Building buildingSite) {
		this.buildingSite = buildingSite;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public Calendar getBornDate() {
		return bornDate;
	}
	public void setBornDate(Calendar bornDate) {
		this.bornDate = bornDate;
	}

	@Override
	public String toString() {
		return "Worker [woId=" + woId + ", chief=" + chief + ", bornDate=" + bornDate + ", buildingSite=" + buildingSite
				+ ", name=" + name + ", surname=" + surname + "]";
	}
	
	
}
