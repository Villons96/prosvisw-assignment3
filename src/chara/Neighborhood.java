//Questa classe rappresenta l'entità Neighborhood della realtà di interesse
package chara;

import javax.persistence.*;

// Definisco il fatto che tale classe è un'entità
@Entity
// Specifico che la generazione e autoincremento della chiave primaria
@SequenceGenerator(name="Neighborhood_Sequence",sequenceName="N_S")
// Ridefinisco il nome della tabella all'interno del DB
@Table(name = "Neighborhood")
public class Neighborhood{

	// Definisco l'attributo che rappresenta la chiave primaria e li cambio il nome
	@Id
	@GeneratedValue (strategy=GenerationType.SEQUENCE, generator="Neighborhood_Sequence")
	@Column(name="\"Id\"")
	private long neighId;
	
	// Definisco l'attributo che rappresenta il nome del quartiere e li cambio il nome
	@Column(name="\"Name\"")
	private String neighName;
	
	// Definisco l'attributo che rappresenta la popolazione del quartiere e li cambio
	// il nome
	@Column(name="\"Population\"")
	private int population;	

	// Costruttore di default
	public Neighborhood() {
		
	}
	
	// Costruttore alternativo
	public Neighborhood(String neighName, int population) {
		this.setNeighName(neighName); 
		this.setPopulation(population); 
	}
	
	// Metodi getters, setters e toString
	public long getNeighId() {
		return neighId;
	}
	public void setNeighId(long neighId) {
		this.neighId = neighId;
	}
	
	public String getNeighName() {
		return neighName;
	}
	public void setNeighName(String neighName) {
		this.neighName = neighName;
	}
	
	public int getPopulation() {
		return population;
	}
	public void setPopulation(int population) {
		this.population = population;
	}

	@Override
	public String toString() {
		return "Neighborhood [neighId=" + neighId + ", neighName=" + neighName + ", population=" + population + "]";
	}
}
