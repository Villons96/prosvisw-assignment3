//Questa classe rappresenta l'entità Building della realtà di interesse

package chara;

import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Vector;
import javax.persistence.*;


// Definisco il fatto che tale classe è un'entità
@Entity
// Specifico che la generazione e autoincremento della chiave primaria
@SequenceGenerator(name="Building_Sequence",sequenceName="B_S")
// Ridefinisco il nome della tabella all'interno del DB
@Table(name = "Building")
public class Building {

	// Definisco l'attributo che rappresenta la chiave primaria e li cambio il nome
	@Id
	@GeneratedValue (strategy=GenerationType.SEQUENCE, generator="Building_Sequence")
	@Column(name="\"Id\"")
	private long buildId;
	
	// Definisco l'attributo che rappresenta la chiave esterna per identificare il 
	// Neighborhood dove il Building ha luogo 
	@ManyToOne(targetEntity=Neighborhood.class) @JoinColumn(name="location")
	private Neighborhood location;
	
	// Definisco l'attributo che identifica la typology del Building e li cambio il nome
	@Column(name="\"Typology\"")
	private String typology;
	
	// Definisco l'attributo che rappresenta la data di inizio lavori
	// Utilizzo @Temporal per definire che si tratta di una data 
	// e li cambio il nome
	@Temporal(TemporalType.DATE)
	@Column(name="\"StartDate\"")
	private GregorianCalendar startDate;
	
	// Definisco l'attributo che rappresenta la data di fine lavori
	// Utilizzo @Temporal per definire che si tratta di una data 
	// e li cambio il nome
	@Temporal(TemporalType.DATE)
	@Column(name="\"EndDate\"")
	private GregorianCalendar endDate;

	// Definisco la relazione N-N tra Building e Company definendo il nome
	// e gli attributi della tabella che verrà creata
	@ManyToMany
	@JoinTable(name = "FundedBy", joinColumns = @JoinColumn(name = "Build_Id"), inverseJoinColumns = @JoinColumn(name = "Company_Id"))
	private Collection<Company> funded;
	
	// Costruttore di default
	public Building() {	
	}
	
	// Costruttore alternativo 
	public Building(Neighborhood locationB, String typologyB, GregorianCalendar startDateB, GregorianCalendar endDateB, Vector<Company> fundedB) {
		this.setLocation(locationB);
		this.setStartDate(startDateB);
		this.setTypology(typologyB);
		this.setEndDate(endDateB);
		this.setFunded(fundedB);	
	}
	
	// Metodi getters, setters e toString
	public Collection<Company> getFunded() {
		return funded;
	}
	public void setFunded(Vector<Company> funded) {
		this.funded = funded;
	}
	
	public Neighborhood getLocation() {
		return location;
	}
	public void setLocation(Neighborhood location) {
		this.location = location;
	}
	
	public long getBuildId() {
		return buildId;
	}
	public void setBuildId(long buildId) {
		this.buildId = buildId;
	}
	
	public String getTypology() {
		return typology;
	}
	public void setTypology(String typology) {
		this.typology = typology;
	}
	
	public GregorianCalendar getStartDate() {
		return startDate;
	}
	public GregorianCalendar getEndDate() {
		return endDate;
	}
	
	public void setStartDate(GregorianCalendar startDate) {
		this.startDate = startDate;
	}
	public void setEndDate(GregorianCalendar endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "Building [buildId=" + buildId + ", location=" + location + ", typology=" + typology + ", startDate="
				+ startDate + ", endDate=" + endDate + ", funded=" + funded + "]";
	}	
}
