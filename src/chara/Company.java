//Questa classe rappresenta l'entità Company della realtà di interesse

package chara;

import javax.persistence.*;

// Definisco il fatto che tale classe è un'entità
@Entity
// Specifico che la generazione e autoincremento della chiave primaria
@SequenceGenerator(name="Company_Sequence",sequenceName="C_S")
// Ridefinisco il nome della tabella all'interno del DB
@Table(name = "Company")
public class Company{

	// Definisco l'attributo che rappresenta la chiave primaria e li cambio il nome
	@Id
	@GeneratedValue (strategy=GenerationType.SEQUENCE, generator="Company_Sequence")
	@Column(name="\"Id\"")
	private long compId;
	
	// Definisco l'attributo che rappresenta il nome della compagnia e li cambio il nome
	@Column(name="\"Name\"")
	private String compName;
	
	// Definisco l'attributo che rappresenta la nazionalità della compagnia e li cambio 
	// il nome
	@Column(name="\"Nationality\"")
	private String compNationality;	
	
	// Costruttore di default
	public Company() {
		
	}
	
	// Costruttore alternativo 
	public Company(String cName, String cNatio) {
		this.setCompName(cName);
		this.setCompNationality(cNatio);
	}
	
	// Metodi getters, setters e toString
	public long getCompId() {
		return compId;
	}
	public void setCompId(long compId) {
		this.compId = compId;
	}
	
	public String getCompName() {
		return compName;
	}
	public void setCompName(String compName) {
		this.compName = compName;
	}
	
	public String getCompNationality() {
		return compNationality;
	}
	public void setCompNationality(String compNationality) {
		this.compNationality = compNationality;
	}

	@Override
	public String toString() {
		return "Company [compId=" + compId + ", compName=" + compName + ", compNationality=" + compNationality + "]";
	}
}
